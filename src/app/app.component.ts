import { Component, OnInit } from "@angular/core";
import { AuthserviceService } from "./services/authservice.service";
import { GeneralserviceService } from "./services/generalservice.service";
import { Alert } from "./interfaces/alert";
import { Title } from "@angular/platform-browser";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.css"],
})
export class AppComponent implements OnInit {
  title = "ESCONFA - Campus Virtual";
  public alert: Alert = { show: false, text: "", type: "", icon: "" };

  constructor(
    public authservice: AuthserviceService,
    public generalservice: GeneralserviceService,
    private titleService: Title
  ) {}

  ngOnInit() {
    this.titleService.setTitle("ESCONFA - Campus Virtual");

    //console.log(this.authservice.isLoggedIn);
    this.generalservice.eventShowAlert.subscribe((options) => {
      console.log(options);
      this.showAlerts(options);
    });
  }

  showAlerts(options: Alert) {
    this.alert = { show: true, ...options };
    setTimeout(() => {
      this.alert.show = false;
    }, 5000);
  }
}
