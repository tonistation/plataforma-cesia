export interface Materiales {
  id?: string;
  name: string;
  url: string;
  type: string;
}
