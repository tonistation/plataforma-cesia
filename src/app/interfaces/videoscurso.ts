export interface Videoscurso {
  id: string;
  name: string;
  active: boolean;
}
