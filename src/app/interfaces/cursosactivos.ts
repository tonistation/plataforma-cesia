import { Alumnoscursos } from "./alumnoscursos";
import { Libroscurso } from "./libroscurso";
import { Materialescurso } from "./materialescurso";
import { Videoscurso } from "./videoscurso";
import { Pruebascurso } from "./pruebascurso";

export interface Cursosactivos {
  id?: string;
  id_curso: string;
  name: string;
  id_coordinador: string;
  name_coordinador: string;
  alumnos: Alumnoscursos[];
  status: number;
  access?: boolean;
  id_area?: string;
  libros?: Libroscurso[];
  materiales?: Materialescurso[];
  videos?: Videoscurso[];
  pruebas?: Pruebascurso[];
  name_especialidad: string;
}
