import { Test } from './test';

export interface Areas {
    name: string,
    description: string,
    id?: string
    test: Test[]
}
