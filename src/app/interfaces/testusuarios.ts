import { PreguntasUsuarios } from "./preguntas-usuarios";
import { Usuarios } from "./usuarios";
import { Timestamp } from "rxjs";

export interface Testusuarios {
  id?: string;
  name: string;
  id_test: string;
  id_area: string;
  id_curso?: string;
  id_user: string;
  questions: PreguntasUsuarios[];
  score: number;
  comments: string;
  status: number;
  dateInitialize: Date;
  dateRealize: Date;
  dateEvaluate: Date;
  userTestData?: Usuarios;
  name_area?: string;
  name_user?: string;
  email_user?: string;
  attachs?: any[];
}
