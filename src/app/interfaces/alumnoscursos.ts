export interface Alumnoscursos {
  id: string;
  identityType: string;
  identity: string;
  firstName: string;
  lastName: string;
  id_user_create?: string;
  name_user_create?: string;
  date_create?: Date;
}
