export interface Libros {
  id?: string;
  name: string;
  description: string;
  type: string;
  id_embed?: string;
}
