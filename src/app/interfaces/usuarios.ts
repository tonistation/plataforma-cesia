import { Areasusuarios } from "./areasusuarios";
import { Cursousuario } from "./cursousuario";

export interface Usuarios {
  id?: string;
  email: string;
  firstName: string;
  lastName: string;
  identity: string;
  identityType: string;
  phone: string;
  photoURL?: string;
  code?: string;
  active?: boolean;
  type?: number;
  areas?: Areasusuarios[];
  cursos?: Cursousuario[];
  comment?: string;
  id_user_create?: string;
  name_user_create?: string;
  date_create?: Date;
  id_user_edit?: string;
  name_user_edit?: string;
  date_edit?: Date;
}
