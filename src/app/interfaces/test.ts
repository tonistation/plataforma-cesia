export interface Test {
  id?: string;
  name: string;
  description: string;
  maxScore: number;
  nroQuestions: number;
  id_bank: string;
  status?: number;
  id_testUser?: string;
  name_area?: string;
  dateInitialize?: Date;
}
