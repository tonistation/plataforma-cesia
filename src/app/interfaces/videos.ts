export interface Videos {
  id?: string;
  name: string;
  description: string;
  url: string;
}
