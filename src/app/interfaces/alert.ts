export interface Alert {
    text: string,
    type: string,
    show: boolean,
    icon?: string
}

