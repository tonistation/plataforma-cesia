import { Libros } from "./libros";
import { Materiales } from "./materiales";
import { Videos } from "./videos";

export interface Cursousuario {
  id_curso?: string;
  name_curso: string;
  libros?: Libros[];
  materiales?: Materiales[];
  videos?: Videos[];
  access?: boolean;
}
