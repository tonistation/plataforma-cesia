import { Areas } from './areas';

export interface FiltersTest {
    area: Areas | string,
    order: string,
    user: string,
    email: string,
}
