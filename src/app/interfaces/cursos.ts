import { Libros } from "./libros";
import { Materiales } from "./materiales";
import { Videos } from "./videos";
import { Test } from "./test";

export interface Cursos {
  id?: string;
  name: string;
  description: string;
  libros: Libros[];
  materiales: Materiales[];
  videos: Videos[];
  pruebas: Test[];
  id_curso?: string;
}
