export interface Libroscurso {
  id: string;
  name: string;
  active: boolean;
}
