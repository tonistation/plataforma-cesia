export interface Pruebascurso {
  id: string;
  name: string;
  active: boolean;
  status?: number;
  id_testUser?: string;
  dateInitialize?: Date;
}
