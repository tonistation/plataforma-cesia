import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { PruebasService } from "src/app/services/pruebas.service";
import { Testusuarios } from "src/app/interfaces/testusuarios";
import * as ClassicEditor from "@ckeditor/ckeditor5-build-classic";
import { GeneralserviceService } from "src/app/services/generalservice.service";
import { UsersService } from "src/app/services/users.service";
import { Usuarios } from "src/app/interfaces/usuarios";
import { Areas } from "src/app/interfaces/areas";
import { Subscription } from "rxjs";
import { AreaserviceService } from "src/app/services/areaservice.service";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { FirebaseStorageService } from "src/app/services/firebase-storage.service";
import { from } from "rxjs";
import { Cursos } from "src/app/interfaces/cursos";
import { CoursesserviceService } from "src/app/services/coursesservice.service";
import { Cursosactivos } from "src/app/interfaces/cursosactivos";

@Component({
  selector: "app-test-evaluate-teacher",
  templateUrl: "./test-evaluate-teacher.component.html",
  styleUrls: ["./test-evaluate-teacher.component.scss"],
})
export class TestEvaluateTeacherComponent implements OnInit {
  public Editor = ClassicEditor;
  public configEditor = { language: "es", isReadOnly: false };
  public inLoad: boolean = true;
  public resultSaveTest = {
    msg: "",
    saved: false,
    inSave: false,
  };

  public modalConfirmEvaluate: boolean = false;
  public cursosData: Cursosactivos[] = [];

  public userTestData: Usuarios = {
    identity: "",
    identityType: "",
    email: "",
    firstName: "",
    lastName: "",
    phone: "",
    cursos: [],
  };

  public pruebaData: Testusuarios = {
    name: "Prueba",
    id_test: "",
    id_area: "",
    id_user: "",
    questions: [],
    score: 0,
    comments: "",
    status: 0,
    dateRealize: null,
    dateEvaluate: null,
    dateInitialize: null,
    attachs: [],
  };

  public archivoForm = new FormGroup({
    archivo: new FormControl(null, Validators.required),
  });
  public mensajeArchivo = "";
  public datosFormulario = new FormData();
  public nombreArchivo = "";
  public URLPublica = "";
  public porcentaje = 0;
  public finalizado = false;

  constructor(
    private route: ActivatedRoute,
    private pruebasservice: PruebasService,
    private generalservice: GeneralserviceService,
    private cursosservice: CoursesserviceService,
    private usersservice: UsersService,
    private firebaseStorage: FirebaseStorageService,
    private router: Router
  ) {}

  ngOnInit() {
    this.pruebaData.attachs = [];
    if (this.route.snapshot.paramMap.get("id") !== null) {
      this.pruebaData.id = this.route.snapshot.paramMap.get("id");
      this.getcursosData();
    }
  }

  loadData() {
    this.inLoad = true;
    this.pruebasservice.get(this.pruebaData.id).subscribe((data) => {
      let dataTemp = data.data() as Testusuarios;
      dataTemp.dateInitialize = this.generalservice.convertDate(
        dataTemp.dateInitialize
      );
      dataTemp.dateEvaluate = this.generalservice.convertDate(
        dataTemp.dateEvaluate
      );
      dataTemp.dateRealize = this.generalservice.convertDate(
        dataTemp.dateRealize
      );

      this.pruebaData = dataTemp;
      this.getDataUserTest();
      if (this.pruebaData.status === 1 || this.pruebaData.status === 2)
        this.configEditor.isReadOnly = true;

      this.cursosData.forEach((area) => {
        if (this.pruebaData.id_curso === area.id) {
          this.pruebaData.name_area = area.name;
        }
      });
      //console.log(this.pruebaData)
      this.inLoad = false;
    });
  }

  getcursosData() {
    let subscripcion: Subscription;
    subscripcion = this.cursosservice.listActives().subscribe(
      (data) => {
        this.cursosData = data as Cursosactivos[];
        this.loadData();
        this.unsubscribeMethod(subscripcion);
      },
      (error) => {
        this.generalservice.eventShowAlert.emit({
          text:
            "Ha ocurrido un error cargando las areas, intente nuevamente o verifique su conexion a internet.",
          type: "error",
        });
      }
    );
  }

  getDataUserTest() {
    this.usersservice.get(this.pruebaData.id_user).subscribe((data) => {
      this.userTestData = data.data() as Usuarios;
      this.pruebaData.userTestData = this.userTestData;
    });
  }

  modalConfirmEvaluateOpen() {
    this.modalConfirmEvaluate = true;
    this.resultSaveTest.msg = "";
    this.resultSaveTest.inSave = false;
    this.resultSaveTest.saved = false;
  }

  saveTestEvaluation() {
    this.resultSaveTest.inSave = true;
    this.pruebaData.status = 2;
    this.pruebaData.dateEvaluate = new Date();
    this.pruebasservice.store(this.pruebaData).then(
      () => {
        this.resultSaveTest.inSave = false;
        this.resultSaveTest.saved = true;
        this.generalservice.eventShowAlert.emit({
          text: "Se ha guardado la evaluacion exitosamente",
          type: "success",
        });
        this.router.navigate(["profesor", "test", "lista"]);
      },
      (error) => {
        this.generalservice.eventShowAlert.emit({
          text:
            "Ha ocurrido un error guardando la evaluacion, intente nuevamente",
          type: "error",
        });
      }
    );
  }

  cambioArchivo(event) {
    if (event.target.files.length > 0) {
      for (let i = 0; i < event.target.files.length; i++) {
        this.mensajeArchivo = `Archivo preparado para subir`;
        this.nombreArchivo = event.target.files[i].name;
        this.datosFormulario.delete("archivo");
        this.datosFormulario.append(
          "archivo",
          event.target.files[i],
          event.target.files[i].name
        );
      }
    } else {
      this.mensajeArchivo = "No hay un archivo seleccionado";
      this.nombreArchivo = "";
    }
  }

  //Sube el archivo a Cloud Storage
  subirArchivo() {
    let archivo = this.datosFormulario.get("archivo");
    let tarea = this.firebaseStorage.tareaCloudStorage(
      this.nombreArchivo,
      archivo
    );

    //Cambia el porcentaje
    tarea.percentageChanges().subscribe((porcentaje) => {
      this.porcentaje = Math.round(porcentaje);
    });

    console.log(this.pruebaData);

    this.pruebaData.attachs =
      this.pruebaData.attachs === undefined || this.pruebaData.attachs == null
        ? []
        : this.pruebaData.attachs;

    tarea.then((snapshot) => {
      console.log(snapshot);
      snapshot.ref.getDownloadURL().then((downloadURL) => {
        this.finalizado = true;
        this.mensajeArchivo = "";
        this.nombreArchivo = "";
        this.generalservice.eventShowAlert.emit({
          text: "Se ha subido el archivo exitosamente",
          type: "success",
        });
        let attachsInsert = { url: downloadURL };
        this.pruebaData.attachs.push(attachsInsert);
      });
    });
  }

  unsubscribeMethod(suscripcion: Subscription) {
    suscripcion.unsubscribe();
  }
}
