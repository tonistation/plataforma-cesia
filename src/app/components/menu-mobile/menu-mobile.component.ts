import { Component, OnInit } from '@angular/core';
import { AuthserviceService } from 'src/app/services/authservice.service';
import { Usuarios } from 'src/app/interfaces/usuarios';

@Component({
  selector: 'app-menu-mobile',
  templateUrl: './menu-mobile.component.html',
  styleUrls: ['./menu-mobile.component.scss']
})
export class MenuMobileComponent implements OnInit {
  public userLoad: boolean = false
  public userData: Usuarios = {
    identity: '',
    identityType: '',
    email: '',
    phone: '',
    firstName: '',
    lastName: '',
    areas: []
  }

  constructor(
    public authservice: AuthserviceService
  ) { }

  ngOnInit() {
    this.getUserData()
  }

  getUserData() {
    setTimeout(() => {
      this.userLoad = true;
      this.userData = this.authservice.getUserData();
    }, 1000)
  }

}
