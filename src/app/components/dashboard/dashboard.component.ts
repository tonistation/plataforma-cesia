import { Component, OnInit } from "@angular/core";
import { AuthserviceService } from "src/app/services/authservice.service";
import { Usuarios } from "src/app/interfaces/usuarios";

@Component({
  selector: "app-dashboard",
  templateUrl: "./dashboard.component.html",
  styleUrls: ["./dashboard.component.scss"],
})
export class DashboardComponent implements OnInit {
  public userData: Usuarios;
  public textSaludo: string;

  constructor(private authservice: AuthserviceService) {}

  ngOnInit() {
    this.getUserData();
    this.mostrarSaludo();
  }

  getUserData() {
    this.userData = this.authservice.getUserData();
  }

  mostrarSaludo() {
    let fecha = new Date();
    let hora = fecha.getHours();

    if (hora >= 0 && hora < 12) {
      this.textSaludo = "buenos días";
    }

    if (hora >= 12 && hora < 18) {
      this.textSaludo = "buenas tardes";
    }

    if (hora >= 18 && hora < 24) {
      this.textSaludo = "buenas noches";
    }
  }
}
