import { Component, OnInit } from "@angular/core";
import { GeneralserviceService } from "src/app/services/generalservice.service";
import { ClrDatagridSortOrder } from "@clr/angular";
import { Cursos } from "src/app/interfaces/cursos";
import { CoursesserviceService } from "src/app/services/coursesservice.service";

@Component({
  selector: "app-courselist",
  templateUrl: "./courselist.component.html",
  styleUrls: ["./courselist.component.scss"],
})
export class CourselistComponent implements OnInit {
  public inLoad: boolean = false;
  public coursesData: Cursos[];
  public descSort = ClrDatagridSortOrder.DESC;

  constructor(
    private coursesservice: CoursesserviceService,
    private generalservice: GeneralserviceService
  ) {}

  ngOnInit() {
    //this.descSort = ClrDatagridSortOrder.ASC;
    this.list();
  }

  list() {
    this.inLoad = true;
    this.coursesservice.list().subscribe(
      (data) => {
        this.coursesData = data as Cursos[];

        this.coursesData.sort(this.compareSortInit);
        this.inLoad = false;
      },
      (error) => {
        this.inLoad = false;
        this.generalservice.eventShowAlert.emit({
          text:
            "Error en la carga de usuarios, verifique la conexion a internet",
          type: "error",
        });
      }
    );
  }

  compareSortInit(a, b) {
    if (a.firstName < b.firstName) {
      return -1;
    }
    if (a.firstName > b.firstName) {
      return 1;
    }
    return 0;
  }
}
