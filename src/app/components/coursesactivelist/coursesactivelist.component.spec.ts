import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CoursesactivelistComponent } from './coursesactivelist.component';

describe('CoursesactivelistComponent', () => {
  let component: CoursesactivelistComponent;
  let fixture: ComponentFixture<CoursesactivelistComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CoursesactivelistComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CoursesactivelistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
