import { Component, OnInit, ElementRef, ViewChild } from "@angular/core";
import { GeneralserviceService } from "src/app/services/generalservice.service";
import { ClrDatagridSortOrder } from "@clr/angular";
import { Cursos } from "src/app/interfaces/cursos";
import { CoursesserviceService } from "src/app/services/coursesservice.service";
import { SwiperConfigInterface, SwiperDirective } from "ngx-swiper-wrapper";
import { Usuarios } from "src/app/interfaces/usuarios";
import { UsersService } from "src/app/services/users.service";
import { Cursosactivos } from "src/app/interfaces/cursosactivos";
import { AngularFirestore } from "@angular/fire/firestore";
import * as _ from "lodash";
import * as XLSX from "xlsx";
import { Subscription } from "rxjs";
import { AuthserviceService } from "src/app/services/authservice.service";
import { Videoscurso } from "src/app/interfaces/videoscurso";
import { Materialescurso } from "src/app/interfaces/materialescurso";
import { Libroscurso } from "src/app/interfaces/libroscurso";
import { PruebasService } from "src/app/services/pruebas.service";

@Component({
  selector: "app-coursesactivelist",
  templateUrl: "./coursesactivelist.component.html",
  styleUrls: ["./coursesactivelist.component.scss"],
})
export class CoursesactivelistComponent implements OnInit {
  @ViewChild(SwiperDirective, { static: false }) directiveRef: SwiperDirective;
  public inLoad: boolean = false;
  public coursesData: Cursosactivos[];
  public coursesDataFull: Cursosactivos[];
  public especialidades: Cursos[];
  public newCurso: Partial<Cursosactivos>;
  public cursoSelect: Cursosactivos;
  public cursoFilter: Cursosactivos;
  public indexCursoSelect: number;
  public usersData: Usuarios[];
  public userInfoData: Partial<Usuarios>;
  public coordData: Usuarios[];
  public disabledEditTitle = true;
  public coordSelect: Usuarios;
  public descSort = ClrDatagridSortOrder.DESC;
  public modalAddAlumno = false;
  public modalEditCoordinador = false;
  public modalStatusCurso = false;
  public modalNuevoCurso = false;
  public modalInfoAlumno = false;
  public modalRemove = false;
  public modalRecursos = false;
  public statusSelect: number;
  public statusSelectText: string;
  public showFormAlum = false;
  public searchActual: string;
  public nameEspecialidad: string;
  public alumnoData: Usuarios;
  public searchBand = false;
  public userData: Usuarios;
  public configSwiper: SwiperConfigInterface = {
    direction: "horizontal",
    slidesPerView: "auto",
    centeredSlides: true,
    keyboard: true,
    navigation: true,
    pagination: true,
  };
  public searchAlumns: string;
  public tabBooks: boolean = true;
  public tabVideos: boolean = false;
  public tabMaterials: boolean = false;
  public tabPruebas: boolean = false;
  public indexSwiper = 0;
  constructor(
    private authservice: AuthserviceService,
    private coursesservice: CoursesserviceService,
    private generalservice: GeneralserviceService,
    private usersservice: UsersService,
    private pruebasservice: PruebasService,
    private afs: AngularFirestore
  ) {}

  ngOnInit() {
    //this.descSort = ClrDatagridSortOrder.ASC;
    this.list();
    this.loadAlumns();
    this.alumnoData = {
      identity: "",
      identityType: "dni",
      email: "",
      firstName: "",
      lastName: "",
      phone: "",
    };

    this.newCurso = {
      id_curso: "",
      name: "",
      status: 1,
    };
    this.userData = this.authservice.getUserData();

    this.userInfoData = {
      identity: "",
      identityType: "",
      firstName: "",
      lastName: "",
      phone: "",
      email: "",
      id_user_create: "",
      name_user_create: "",
    };
  }

  updateIndexSwipper() {
    this.cursoSelect = this.coursesData[this.directiveRef.getIndex()];
    this.obtieneEspecialidad(this.cursoSelect.id_curso);
    this.statusSelectText =
      this.cursoSelect.status == 1 ? "activo" : "inactivo";
  }

  getListCoord() {
    this.coordData = [];
    this.usersservice.getBy("type", 4).subscribe((data) => {
      data.docs.forEach((row) => {
        let temp = row.data() as Usuarios;
        this.coordData.push(temp);
      });
    });
  }

  getListEspecialidades() {
    this.coursesservice.list().subscribe(
      (data) => {
        this.especialidades = data as Cursos[];
        this.usersData.sort(this.compareSortInit);
        this.inLoad = false;
        this.obtieneEspecialidad(this.coursesData[0].id_curso);
      },
      (error) => {
        this.inLoad = false;
        this.generalservice.eventShowAlert.emit({
          text:
            "Error en la carga de especialidades, verifique la conexion a internet",
          type: "error",
        });
      }
    );
  }

  obtieneEspecialidad(id_curso: string) {
    const esp = this.especialidades.filter((e) => e.id == id_curso);

    if (esp.length > 0) {
      this.nameEspecialidad = esp[0].name;
    } else {
      this.nameEspecialidad = "";
      this.generalservice.eventShowAlert.emit({
        text: "No se encontro la especialidad de este curso",
        type: "error",
      });
    }
  }

  openModalNuevoCurso() {
    this.modalNuevoCurso = true;
  }

  list() {
    this.coursesData = [];
    this.inLoad = true;
    let subscripcion: Subscription;
    subscripcion = this.coursesservice.listActives().subscribe(
      (data) => {
        if (data.length > 0) {
          let thedata = data as Cursosactivos[];

          if (this.userData.type == 1) {
            this.coursesData = thedata;
          } else {
            this.coursesData = thedata.filter((e) => {
              return e.id_coordinador == this.userData.id;
            });
          }

          this.coursesData.sort(this.compareSortInit);
          this.coursesDataFull = _.cloneDeep(this.coursesData);

          this.coursesData.forEach((curso) => {
            if (curso.alumnos.length == undefined) curso.alumnos = [];
            curso.alumnos.forEach((e) => {
              if (e.date_create !== undefined)
                e.date_create = this.generalservice.convertDate(e.date_create);
            });
          });

          this.inLoad = false;
          this.cursoSelect = this.coursesData[0];

          if (this.cursoSelect.videos === undefined)
            this.cursoSelect.videos = [];

          if (this.cursoSelect.libros === undefined)
            this.cursoSelect.libros = [];

          if (this.cursoSelect.materiales === undefined)
            this.cursoSelect.materiales = [];

          if (this.cursoSelect.pruebas === undefined)
            this.cursoSelect.pruebas = [];

          this.unsubscribeMethod(subscripcion);
        }

        this.getListCoord();
        this.getListEspecialidades();
      },
      (error) => {
        this.inLoad = false;
        this.generalservice.eventShowAlert.emit({
          text:
            "Error en la carga de usuarios, verifique la conexion a internet",
          type: "error",
        });
      }
    );
  }

  compareSortInit(a, b) {
    if (a.name < b.name) {
      return -1;
    }
    if (a.name > b.name) {
      return 1;
    }
    return 0;
  }

  getUseById(id: string) {
    if (this.cursoSelect.id_coordinador !== undefined)
      return this.usersData.filter((e) => e.id == id);
    else return false;
  }

  loadAlumns() {
    this.inLoad = true;
    this.usersservice.list().subscribe(
      (data) => {
        this.usersData = data as Usuarios[];
        this.usersData.sort(this.compareSortInit);
        this.inLoad = false;
      },
      (error) => {
        this.inLoad = false;
        this.generalservice.eventShowAlert.emit({
          text:
            "Error en la carga de usuarios, verifique la conexion a internet",
          type: "error",
        });
      }
    );
  }

  editTitle(curso: Cursosactivos) {
    this.disabledEditTitle = !this.disabledEditTitle;

    window.setTimeout(() => {
      document.getElementById("it_" + curso.id).focus();
    }, 0);
  }

  saveTitle(e, curso: Cursosactivos) {
    if ((e.type == "keyup" && e.key == "Enter") || e.type == "change") {
      this.coursesservice.editNameActive(curso);
    }
  }

  saveCoordinador() {
    if (this.coordSelect.id == "") {
      this.cursoSelect.id_coordinador = "";
      this.cursoSelect.name_coordinador = "";
    } else {
      this.cursoSelect.id_coordinador = this.coordSelect.id;
      this.cursoSelect.name_coordinador =
        this.coordSelect.firstName + " " + this.coordSelect.lastName;
    }
    const dataUpdate: Partial<Cursosactivos> = {
      id_coordinador: this.cursoSelect.id_coordinador,
      name_coordinador: this.cursoSelect.name_coordinador,
    };
    this.coursesservice
      .editActive(this.cursoSelect.id, dataUpdate)
      .then((e) => {
        this.generalservice.eventShowAlert.emit({
          text: "Se ha actualizado el coordinador correctamente",
          type: "success",
        });
      })
      .catch((e) => {
        this.generalservice.eventShowAlert.emit({
          text: "Ha ocurrido un error, intente nuevamente",
          type: "error",
        });
      });
    this.modalEditCoordinador = false;
  }

  modalCambioStatus() {
    this.statusSelect = this.cursoSelect.status;
    this.modalStatusCurso = true;
  }

  saveStatus() {
    const dataUpdate: Partial<Cursosactivos> = {
      status: this.statusSelect,
    };
    console.log(dataUpdate, this.cursoSelect);

    this.coursesservice
      .editActive(this.cursoSelect.id, dataUpdate)
      .then((e) => {
        this.coursesData[
          this.directiveRef.getIndex()
        ].status = this.statusSelect;
        this.generalservice.eventShowAlert.emit({
          text: "Se ha actualizado el estado correctamente",
          type: "success",
        });
      })
      .catch((e) => {
        this.generalservice.eventShowAlert.emit({
          text: "Ha ocurrido un error, intente nuevamente",
          type: "error",
        });
      });
    this.modalStatusCurso = false;
  }

  modalCoordinador(curso: Cursosactivos) {
    this.cursoSelect = curso;
    if (this.cursoSelect.videos === undefined) this.cursoSelect.videos = [];

    if (this.cursoSelect.libros === undefined) this.cursoSelect.libros = [];

    if (this.cursoSelect.materiales === undefined)
      this.cursoSelect.materiales = [];

    if (this.cursoSelect.pruebas === undefined) this.cursoSelect.pruebas = [];

    let existCoord = this.coordData.filter(
      (elem) => elem.id == curso.id_coordinador
    );
    if (existCoord.length > 0) {
      this.coordSelect = existCoord[0];
    }
    this.modalEditCoordinador = true;
  }

  openModalRemove(curso: Cursosactivos) {
    this.modalRemove = true;
  }

  confirmDeleteCurso() {
    this.coursesservice
      .deleteActive(this.cursoSelect.id)
      .then((e) => {
        this.generalservice.eventShowAlert.emit({
          text: "Se ha eliminado el curso correctamente",
          type: "success",
        });
        this.list();

        this.modalRemove = true;
      })
      .catch((e) => {
        this.generalservice.eventShowAlert.emit({
          text: "Ha ocurrido un error, intente nuevamente",
          type: "error",
        });

        this.modalRemove = true;
      });
  }

  modalMatricular(index, curso) {
    this.alumnoData = {
      id: "",
      identity: "",
      identityType: "dni",
      email: "",
      firstName: "",
      lastName: "",
      phone: "",
      type: 2,
      active: true,
      id_user_create: this.userData.id,
      name_user_create: this.userData.firstName + " " + this.userData.lastName,
    };

    this.showFormAlum = false;
    this.modalAddAlumno = true;
    this.cursoSelect = curso;
    if (this.cursoSelect.videos === undefined) this.cursoSelect.videos = [];

    if (this.cursoSelect.libros === undefined) this.cursoSelect.libros = [];

    if (this.cursoSelect.materiales === undefined)
      this.cursoSelect.materiales = [];

    if (this.cursoSelect.pruebas === undefined) this.cursoSelect.pruebas = [];
    this.indexCursoSelect = index;
  }

  matricular() {
    if (this.alumnoData.id) {
      this.alumnoData.id_user_create = this.userData.id;
      this.alumnoData.name_user_create =
        this.userData.firstName + " " + this.userData.lastName;
      this.alumnoData.date_create = new Date();

      const {
        identityType,
        identity,
        firstName,
        lastName,
        id,
        id_user_create,
        name_user_create,
        date_create,
      } = this.alumnoData;
      this.coursesData[this.indexCursoSelect].alumnos.push({
        identityType,
        identity,
        firstName,
        lastName,
        id,
        id_user_create,
        name_user_create,
        date_create,
      });

      const data = { alumnos: this.cursoSelect.alumnos };
      console.log(data);

      this.coursesservice
        .storeInCursoActivo(this.cursoSelect.id, data)
        .then(() => {
          this.generalservice.eventShowAlert.emit({
            text: "Se ha matriculado correctamente",
            type: "success",
          });
        })
        .catch(() => {
          this.generalservice.eventShowAlert.emit({
            text: "Ha ocurrido un error, intente nuevamente",
            type: "error",
          });
        });

      this.modalAddAlumno = false;
    } else {
      const id = this.afs.createId();
      this.alumnoData.id = id;
      this.alumnoData.id_user_create = this.userData.id;
      this.alumnoData.name_user_create =
        this.userData.firstName + " " + this.userData.lastName;
      this.alumnoData.date_create = new Date();

      this.usersservice.store(this.alumnoData).then(
        () => {
          const {
            identityType,
            identity,
            firstName,
            lastName,
            id,
            id_user_create,
            name_user_create,
            date_create,
          } = this.alumnoData;
          this.coursesData[this.indexCursoSelect].alumnos.push({
            identityType,
            identity,
            firstName,
            lastName,
            id,
            id_user_create,
            name_user_create,
            date_create,
          });
          const data = { alumnos: this.cursoSelect.alumnos };
          this.coursesservice
            .storeInCursoActivo(this.cursoSelect.id, data)
            .then(() => {
              this.generalservice.eventShowAlert.emit({
                text: "Se ha matriculado correctamente",
                type: "success",
              });
            })
            .catch(() => {
              this.generalservice.eventShowAlert.emit({
                text: "Ha ocurrido un error, intente nuevamente",
                type: "error",
              });
            });
          this.modalAddAlumno = false;
        },
        (error) => {
          this.generalservice.eventShowAlert.emit({
            text: "Error en creación de usuario",
            type: "danger",
          });
        }
      );
    }
  }

  desMatricular(index: number, indexAlumn: number, curso: Cursosactivos) {
    //console.log(this.coursesData[index]);
    this.coursesData[index].alumnos.splice(indexAlumn, 1);
    //console.log(indexAlumn, this.coursesData[index].alumnos);

    const data = { alumnos: this.coursesData[index].alumnos };
    this.coursesservice.storeInCursoActivo(this.coursesData[index].id, data);
    this.generalservice.eventShowAlert.emit({
      text: "Se ha desincorporado correctamente",
      type: "success",
    });
  }

  buscarAlumno() {
    this.usersservice
      .validIdentity(this.alumnoData.identityType, this.alumnoData.identity)
      .subscribe((dataIdentity) => {
        if (dataIdentity.docs.length > 0) {
          this.usersservice.load(dataIdentity.docs[0].id).subscribe((val) => {
            this.alumnoData = val.data() as Usuarios;
          });
        }
        this.showFormAlum = true;
      });
  }

  excelReporte(curso: Cursosactivos) {
    /* table id is passed over here */

    let element = document.getElementById("tabla_" + curso.id);
    const ws: XLSX.WorkSheet = XLSX.utils.table_to_sheet(element);

    /* generate workbook and add the worksheet */
    const wb: XLSX.WorkBook = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(wb, ws, "Alumnos");

    /* save to file */
    XLSX.writeFile(wb, curso.name + ".xlsx");
  }

  saveNewCurso() {
    let data = {
      name: this.newCurso.name,
      id_curso: this.newCurso.id_curso,
      alumnos: [],
      status: 1,
    };

    this.coursesservice
      .storeCursoActivo(data)
      .then(() => {
        this.generalservice.eventShowAlert.emit({
          text: "Curso creado correctamente",
          type: "success",
        });
        this.list();

        this.modalNuevoCurso = false;
      })
      .catch(() => {
        this.generalservice.eventShowAlert.emit({
          text: "Ha ocurrido un error, intente nuecamente",
          type: "error",
        });

        this.modalNuevoCurso = false;
      });

    this.newCurso = {
      id_curso: "",
      name: "",
      status: 1,
    };
  }

  selectionChanged(selectItem) {
    let cont = 0;
    this.coursesDataFull.forEach((e) => {
      if (e.id == selectItem.value.id) {
        this.directiveRef.setIndex(cont);
      }
      cont++;
    });
  }

  selectEvent(item) {
    this.searchBand = true;
    this.coursesData = _.cloneDeep(this.coursesDataFull).filter(
      (e) => e.id == item.id
    );
    console.log(this.coursesData);
    console.log("select");

    this.directiveRef.update();
  }

  clearedSearch() {
    this.coursesData = _.cloneDeep(this.coursesDataFull);
    this.directiveRef.update();
  }

  closedSearch() {
    if (this.searchBand) {
      this.searchBand = false;
    } else {
      this.coursesData = this.coursesDataFull.filter((e) =>
        e.name.toLowerCase().includes(this.searchActual.toLowerCase())
      );
      this.directiveRef.update();
    }
  }

  onChangeSearch(val: string) {
    this.searchActual = val;
  }

  onFocused(e) {
    // do something when input is focused
  }
  unsubscribeMethod(suscripcion: Subscription) {
    suscripcion.unsubscribe();
  }

  createCursos() {
    let subscripcion: Subscription;
    subscripcion = this.coursesservice.list().subscribe(
      (data) => {
        let thedata = data as Cursos[];
        thedata.forEach((e) => {
          this.showAlumns(e);
        });

        this.unsubscribeMethod(subscripcion);
      },
      (error) => {
        this.inLoad = false;
        this.generalservice.eventShowAlert.emit({
          text:
            "Error en la carga de usuarios, verifique la conexion a internet",
          type: "error",
        });
      }
    );
  }

  showAlumns(curso: Cursos) {
    let temp = [];
    this.usersData.forEach((user: Usuarios) => {
      if (user.cursos !== undefined) {
        user.cursos.forEach((cursoUser) => {
          if (cursoUser.id_curso == curso.id) {
            const { identityType, identity, firstName, lastName, id } = user;
            temp.push({ identityType, identity, firstName, lastName, id });
          }
        });
        this.inLoad = false;
      }
    });
    //console.log(temp);
    /*
    if (temp.length > 0) {
      let data = {
        name: curso.name + " 2020",
        id_curso: curso.id,
        alumnos: temp,
        status: 1,
        id_coordinador: "",
        name_coordinador: "",
      };

      this.coursesservice.storeCursoActivo(data);
    }*/
  }

  infoUser(alumno: Usuarios) {
    this.userInfoData = {
      identity: "",
      identityType: "",
      firstName: "",
      lastName: "",
      phone: "",
      email: "",
    };
    this.modalInfoAlumno = true;
    this.usersservice.get(alumno.id).subscribe((data) => {
      this.userInfoData = data.data();
    });
  }

  openModalRecursos(index: number, curso: Cursosactivos) {
    this.modalRecursos = true;
    this.indexCursoSelect = index;
  }

  refreshRecursos() {
    //console.log(this.cursoSelect.id_curso);

    this.coursesservice.get(this.cursoSelect.id_curso).subscribe(
      (data) => {
        if (data.exists) {
          const temp = data.data() as Cursos;
          this.coursesData[this.indexCursoSelect].name_especialidad = temp.name;

          let videos: Videoscurso[] = [];
          temp.videos.forEach((elem) => {
            let { id, name } = elem;
            videos.push({ id: id, name: name, active: true });
          });

          this.coursesData[this.indexCursoSelect].videos = temp.videos
            ? videos
            : [];

          let materiales: Materialescurso[] = [];
          temp.materiales.forEach((elem) => {
            let { id, name } = elem;
            materiales.push({ id: id, name: name, active: true });
          });

          this.coursesData[this.indexCursoSelect].materiales = temp.materiales
            ? materiales
            : [];

          let libros: Libroscurso[] = [];
          temp.libros.forEach((elem) => {
            let { id, name } = elem;
            libros.push({ id: id, name: name, active: true });
          });
          this.coursesData[this.indexCursoSelect].libros = temp.libros
            ? libros
            : [];

          let pruebas: Libroscurso[] = [];
          temp.pruebas.forEach((elem) => {
            let { id, name } = elem;
            pruebas.push({ id: id, name: name, active: true });
          });
          this.coursesData[this.indexCursoSelect].pruebas = temp.pruebas
            ? pruebas
            : [];
          this.cursoSelect = this.coursesData[this.indexCursoSelect];
        } else {
          this.generalservice.eventShowAlert.emit({
            text:
              "La especialidad vinculada a este curso ya no existe, (Recomendacion: Eliminar este curso).",
            type: "error",
          });
        }
      },
      (error) => {}
    );
  }

  saveEstatusRecursos() {
    const data = {
      videos: this.cursoSelect.videos,
      libros: this.cursoSelect.libros,
      materiales: this.cursoSelect.materiales,
      pruebas: this.cursoSelect.pruebas,
    };

    this.coursesservice
      .storeInCursoActivo(this.cursoSelect.id, data)
      .then(() => {
        this.generalservice.eventShowAlert.emit({
          text: "Se ha guardado correctamente",
          type: "success",
        });
        this.modalRecursos = false;
      })
      .catch(() => {
        this.generalservice.eventShowAlert.emit({
          text: "Ha ocurrido un error, intente nuevamente",
          type: "error",
        });
      });
  }

  fixes() {
    this.cursoSelect.alumnos.forEach((a) => {
      this.pruebasservice.getBy("id_user", a.id).subscribe((r) => {
        let data = r.docs;
        let pruebasCheck = [];
        data.forEach((d) => {
          let temp = d.data();
          //console.log(temp.id_area, this.cursoSelect.id_area);
          //console.log(temp.id_curso, this.cursoSelect.id_curso);
          if (
            temp.id_area == this.cursoSelect.id ||
            temp.id_curso == this.cursoSelect.id ||
            temp.id_area == this.cursoSelect.id_curso
          ) {
            let datau = {
              id_curso: this.cursoSelect.id,
              id_area: this.cursoSelect.id_curso,
            };
            //console.log("update", temp);
            this.pruebasservice.update(temp.id, datau).then(() => {
              console.log("actualizado", temp.id);
            });
          }
        });
      });
    });
  }
}
