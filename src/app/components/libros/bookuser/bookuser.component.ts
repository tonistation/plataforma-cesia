import { Component, OnInit } from "@angular/core";
import { BooksService } from "src/app/services/books.service";
import { Libros } from "src/app/interfaces/libros";

@Component({
  selector: "app-bookuser",
  templateUrl: "./bookuser.component.html",
  styleUrls: ["./bookuser.component.scss"]
})
export class BookuserComponent implements OnInit {
  public booksData: Libros[];
  constructor(private bookservice: BooksService) {}

  ngOnInit() {
    this.load();
  }

  load() {
    this.bookservice.list().subscribe(data => {
      this.booksData = data as Libros[];
    });
  }
}
