import { Component, OnInit } from "@angular/core";
import { PruebasService } from "src/app/services/pruebas.service";
import { AuthserviceService } from "src/app/services/authservice.service";
import { AreaserviceService } from "src/app/services/areaservice.service";
import { Areas } from "src/app/interfaces/areas";
import { Usuarios } from "src/app/interfaces/usuarios";
import { Areasusuarios } from "src/app/interfaces/areasusuarios";
import { Test } from "src/app/interfaces/test";
import { Testusuarios } from "src/app/interfaces/testusuarios";
import { BanksService } from "src/app/services/banks.service";
import { GeneralserviceService } from "src/app/services/generalservice.service";
import { Router, ActivatedRoute } from "@angular/router";
import { AngularFirestore } from "@angular/fire/firestore";
import { PreguntasUsuarios } from "src/app/interfaces/preguntas-usuarios";
import { Subscription } from "rxjs";
import { UsersService } from "src/app/services/users.service";
import { FiltersTest } from "src/app/interfaces/filters-test";
import { Cursos } from "src/app/interfaces/cursos";
import { CoursesserviceService } from "src/app/services/coursesservice.service";
import { Cursosactivos } from "src/app/interfaces/cursosactivos";

@Component({
  selector: "app-test-list-teacher",
  templateUrl: "./test-list-teacher.component.html",
  styleUrls: ["./test-list-teacher.component.scss"],
})
export class TestListTeacherComponent implements OnInit {
  public tabTestPending: boolean = true;
  public tabTestHistory: boolean = false;
  public inLoad: boolean = false;
  public p: number;

  public userData: Usuarios = {
    identity: "",
    identityType: "",
    email: "",
    phone: "",
    firstName: "",
    lastName: "",
    areas: [],
  };

  public areasTestUser: Cursos[] = [];
  public testPendingData: Testusuarios[] = [];
  public testHistoryData: Testusuarios[] = [];
  public cursosData: Cursosactivos[] = [];
  public usersData: Usuarios[] = [];

  public filters: FiltersTest = {
    area: "",
    order: "new",
    user: "",
    email: "",
  };

  public modalConfirmTestStart: boolean = false;
  public cursoSelData: Cursos = {
    name: "",
    description: "",
    pruebas: [],
    libros: [],
    materiales: [],
    videos: [],
  };
  public testSelData: Test = {
    name: "",
    description: "",
    maxScore: 0,
    nroQuestions: 0,
    id_bank: "",
  };
  public inGenerateTest: boolean = false;

  constructor(
    private authservice: AuthserviceService,
    private cursosservice: CoursesserviceService,
    private bankservice: BanksService,
    private pruebasservice: PruebasService,
    private generalservice: GeneralserviceService,
    private router: Router,
    private route: ActivatedRoute,
    private usersservice: UsersService,
    private afs: AngularFirestore
  ) {}

  ngOnInit() {
    this.getUserData();
  }

  getUserData() {
    this.inLoad = true;

    this.getUsersData();
    this.userData = this.authservice.getUserData();
  }

  getcursosData() {
    //console.log("getcursosData");
    let subscripcion: Subscription;

    subscripcion = this.cursosservice
      .listActivesByTypeUser(this.userData.id, this.userData.type)
      .subscribe(
        (data) => {
          this.cursosData = data as Cursosactivos[];

          // console.log("cursos", this.cursosData);

          if (this.route.snapshot.paramMap.get("tab") !== null) {
            if (this.route.snapshot.paramMap.get("tab") === "historial") {
              this.tabTestPending = false;
              this.tabTestHistory = true;
              this.getTestsDataHistory();
            } else {
              this.getTestsDataPending();
            }
          } else {
            this.getTestsDataPending();
          }
          this.unsubscribeMethod(subscripcion);
        },
        (error) => {
          this.generalservice.eventShowAlert.emit({
            text:
              "Ha ocurrido un error cargando las pruebas, intente nuevamente o verifique su conexion a internet.",
            type: "error",
          });
        }
      );
  }

  getTestsDataPending() {
    //console.log("atd1");
    this.filters = {
      area: "",
      order: "new",
      user: "",
      email: "",
    };
    let subscripcion: Subscription;
    this.testPendingData = [];
    subscripcion = this.pruebasservice.getTestsPendingEvaluate().subscribe(
      (data) => {
        let dataTemp = data as Testusuarios[];
        console.log("tu", dataTemp, this.userData);

        dataTemp.forEach((test) => {
          var evaluate = false;

          let dateTest = this.generalservice.convertDate(test.dateInitialize);
          const dateLimitNewUpdate = new Date(2020, 4, 31);

          if (dateTest >= dateLimitNewUpdate) {
            if (this.userData.type === 3) {
              evaluate = this.cursosData.some(
                (elem) => elem.id == test.id_curso
              );
            } else if (this.userData.type === 4) {
              this.cursosData.forEach((elem) => {
                console.log(elem, test);
                if (elem.id_curso == test.id_area) {
                  console.log("LOCURA");
                }
              });

              evaluate = this.cursosData.some(
                (elem) =>
                  elem.id == test.id_curso || elem.id_curso == test.id_area
              );
            } else {
              evaluate = true;
            }
          } else {
            evaluate =
              this.userData.type == 4
                ? this.cursosData.some((elem) => elem.id_curso == test.id_area)
                : true;
          }

          console.log("cumple", test, evaluate);

          if (evaluate) {
            test.dateInitialize = this.generalservice.convertDate(
              test.dateInitialize
            );
            test.dateEvaluate = this.generalservice.convertDate(
              test.dateEvaluate
            );
            test.dateRealize = this.generalservice.convertDate(
              test.dateRealize
            );

            console.log(this.cursosData);

            this.cursosData.forEach((area) => {
              console.log(
                test.id,
                test.id_area,
                test.id_curso,
                area.id,
                area.id_area,
                area.name,
                area.id_curso
              );

              //area.id_area = area.id_curso;
              //console.log(test.dateInitialize, dateLimitNewUpdate);

              if (
                test.id_area == area.id ||
                test.id_curso == area.id ||
                test.id_curso == area.id_curso ||
                test.id_area == area.id_curso
              ) {
                test.name_area = area.name;
              }
            });
            this.testPendingData.push(test);
          }
        });

        //this.testPendingData = dataTemp;
        this.orderList();
        this.getDataUserTestP();
        this.getTestsDataHistory();
        this.unsubscribeMethod(subscripcion);
      },
      (error) => {
        this.generalservice.eventShowAlert.emit({
          text: "Error obteniendo historial de pruebas pendientes",
          type: "error",
        });
      }
    );
  }

  getUsersData() {
    let subscripcion: Subscription;
    subscripcion = this.usersservice.list_get().subscribe(
      (data) => {
        let temp = [];
        data.docs.map((d) => {
          temp.push(d.data());
        });

        this.usersData = temp as Usuarios[];

        //console.log("getUsersData");
        this.getcursosData();
        this.unsubscribeMethod(subscripcion);
      },
      (error) => {
        this.generalservice.eventShowAlert.emit({
          text: "Error obteniendo información de usuarios de las pruebas",
          type: "error",
        });
      }
    );
  }

  getDataUserTestP() {
    //console.log(this.testPendingData);
    //console.log(this.usersData);

    this.testPendingData.forEach((test) => {
      this.usersData.forEach((user) => {
        //console.log(user.id, test.id_user);
        if (user.id === test.id_user) {
          test.userTestData = user;
          test.name_user = user.lastName + " " + user.firstName;
          test.email_user = user.email;
        }
      });
    });
  }

  getDataUserTestH(userid: string, index: number) {
    this.usersData.forEach((user) => {
      if (user.id === this.testHistoryData[index].id_user) {
        this.testHistoryData[index].userTestData = user;
        this.testHistoryData[index].name_user =
          user.lastName + " " + user.firstName;
        this.testHistoryData[index].email_user = user.email;
      }
    });
  }

  confirmTest(area: Cursos, test: Test) {
    this.modalConfirmTestStart = true;
    this.cursoSelData = area;
    this.testSelData = test;
  }

  saveTestUser(data) {
    let idTest = this.afs.createId();
    data.id = idTest;
    this.pruebasservice.store(data).then(
      () => {
        this.generalservice.eventShowAlert.emit({
          text: "Prueba generada exitosamente",
          type: "success",
        });
        this.router.navigate(["test", "realizar", idTest]);
      },
      (error) => {
        this.generalservice.eventShowAlert.emit({
          text: "Error genrando la prueba",
          type: "error",
        });
      }
    );
  }

  getTestsDataHistory() {
    this.filters = {
      area: "",
      order: "new",
      user: "",
      email: "",
    };
    let subscripcion: Subscription;
    subscripcion = this.pruebasservice.getTestsHistoryEvaluate().subscribe(
      (data) => {
        this.testHistoryData = [];
        let dataTemp = data as Testusuarios[];
        dataTemp.forEach((dt) => {
          var evaluate = false;

          let dateTest = this.generalservice.convertDate(dt.dateInitialize);
          const dateLimitNewUpdate = new Date(2020, 4, 31);
          // console.log(dateTest);

          if (dateTest >= dateLimitNewUpdate) {
            evaluate =
              this.userData.type == 4
                ? this.cursosData.some((elem) => elem.id == dt.id_curso)
                : true;
          } else {
            evaluate =
              this.userData.type == 4
                ? this.cursosData.some((elem) => elem.id_curso == dt.id_area)
                : true;
          }

          if (evaluate) {
            dt.dateInitialize = this.generalservice.convertDate(
              dt.dateInitialize
            );
            dt.dateEvaluate = this.generalservice.convertDate(dt.dateEvaluate);
            dt.dateRealize = this.generalservice.convertDate(dt.dateRealize);

            this.cursosData.forEach((area) => {
              area.id_area = area.id_curso;
              //console.log("aca", dt.dateInitialize, dateLimitNewUpdate);
              if (dt.dateInitialize >= dateLimitNewUpdate) {
                if (dt.id_area == area.id || dt.id_curso == area.id) {
                  dt.name_area = area.name;
                }
              } else {
                if (dt.id_area == area.id_curso) {
                  dt.name_area = area.name;
                }
              }
            });

            this.testHistoryData.push(dt);
          }
        });

        //this.testHistoryData = dataTemp;
        this.orderListEvaluate();
        this.testHistoryData.forEach((test: Testusuarios, index: number) => {
          this.getDataUserTestH(test.id_user, index);
          this.cursosData.forEach((area) => {
            //area.id_area = area.id_curso;
            if (test.id_area == area.id_curso) {
              test.name_area = area.name;
            }
          });
        });
        //console.log(this.testHistoryData)
        this.unsubscribeMethod(subscripcion);
      },
      (error) => {
        this.generalservice.eventShowAlert.emit({
          text: "Error obteniendo historial de pruebas",
          type: "error",
        });
      }
    );
  }

  orderList() {
    if (this.filters.order == "old") {
      let cloneData = [...this.testPendingData];
      // console.log(cloneData);
      cloneData.sort((a: any, b: any) =>
        a.dateRealize.getTime() > b.dateRealize.getTime()
          ? 1
          : b.dateRealize.getTime() > a.dateRealize.getTime()
          ? -1
          : 0
      );
      this.testPendingData = [...cloneData];
    } else {
      if (this.filters.order == "new") {
        let cloneData = [...this.testPendingData];
        // console.log(cloneData);
        cloneData.sort((a: any, b: any) =>
          a.dateRealize.getTime() < b.dateRealize.getTime()
            ? 1
            : b.dateRealize.getTime() < a.dateRealize.getTime()
            ? -1
            : 0
        );
        this.testPendingData = [...cloneData];
      }
    }
  }

  orderListEvaluate() {
    if (this.filters.order == "old") {
      let cloneData = [...this.testHistoryData];
      //console.log(cloneData);
      cloneData.sort((a: any, b: any) =>
        a.dateEvaluate.getTime() > b.dateEvaluate.getTime()
          ? 1
          : b.dateEvaluate.getTime() > a.dateEvaluate.getTime()
          ? -1
          : 0
      );
      this.testHistoryData = [...cloneData];
    } else {
      if (this.filters.order == "new") {
        let cloneData = [...this.testHistoryData];
        console.log(cloneData);
        cloneData.sort((a: any, b: any) =>
          a.dateEvaluate.getTime() < b.dateEvaluate.getTime()
            ? 1
            : b.dateEvaluate.getTime() < a.dateEvaluate.getTime()
            ? -1
            : 0
        );
        this.testHistoryData = [...cloneData];
      }
    }
  }

  unsubscribeMethod(suscripcion: Subscription) {
    suscripcion.unsubscribe();
  }
}
