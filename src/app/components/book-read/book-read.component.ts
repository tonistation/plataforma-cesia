import { Component, OnInit } from "@angular/core";
import { BooksService } from "src/app/services/books.service";
import { Libros } from "src/app/interfaces/libros";
import { Router, ActivatedRoute } from "@angular/router";
import { log } from "util";
declare var Yumpu: any;

@Component({
  selector: "app-bookread",
  templateUrl: "./book-read.component.html",
  styleUrls: ["./book-read.component.scss"],
})
export class BookReadComponent implements OnInit {
  public bookData: Partial<Libros> = {};
  public inLoad: boolean = true;
  Yumpu: any;
  constructor(
    private route: ActivatedRoute,
    private booksservice: BooksService,
    private router: Router
  ) {}

  ngOnInit() {
    if (this.route.snapshot.paramMap.get("code") !== null) {
      this.bookData.id = this.route.snapshot.paramMap.get("code");
      this.loadData();
    }
  }

  loadData() {
    this.inLoad = true;
    this.booksservice.get(this.bookData.id).subscribe((data) => {
      this.bookData = data.data() as Libros;
      var myYumpu = new Yumpu();
      myYumpu.create_player("#yumpuMagazineContainer", "", {
        lang: "es",
        embed_id: this.bookData.id_embed,
        embedded: true,
      });

      this.inLoad = false;
    });
  }

  noCMVideo(e) {
    e.preventDefault();
    return false;
  }
}
