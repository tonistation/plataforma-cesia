import { Component, OnInit, ViewChild, ElementRef } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { Areas } from "src/app/interfaces/areas";
import { AreaserviceService } from "src/app/services/areaservice.service";
import { Test } from "src/app/interfaces/test";
import { GeneralserviceService } from "src/app/services/generalservice.service";
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic'; import { Bancos } from 'src/app/interfaces/bancos';
import { Preguntas } from 'src/app/interfaces/preguntas';
import { BanksService } from 'src/app/services/banks.service';

@Component({
  selector: "app-form-areas",
  templateUrl: "./form-areas.component.html",
  styleUrls: ["./form-areas.component.scss"]
})
export class FormAreasComponent implements OnInit {

  @ViewChild("bankSector", { static: false }) bankSectorHTML: ElementRef;
  public configEditor = { language: 'es' }

  public areaData: Areas = {
    id: "",
    name: "",
    description: "",
    test: []
  };

  public newTest: Test = {
    name: "",
    description: "",
    maxScore: 20,
    nroQuestions: 10,
    id_bank: ''
  };

  public listTestDelete: Test[] = new Array()

  public bankSelect: Bancos = {
    questions: []
  }

  public questionData: Preguntas = {
    type: 'open',
    text: '',
    id: ''
  }

  public disableForm: boolean = false
  public disableFormTest: boolean = false
  public inNewTest: boolean = false
  public inNewQuestion: boolean = false
  private indexTestEdit: number
  private indexQuestionEdit: number
  public modalDeletePrueba: boolean = false
  public modalConfirmDelete: boolean = false
  public modalQuestion: boolean = false
  public modalConfirmDeleteQuestion: boolean = false
  public inBank: boolean = false
  public Editor = ClassicEditor


  constructor(
    private route: ActivatedRoute,
    private areasservice: AreaserviceService,
    private banksservice: BanksService,
    private generalservice: GeneralserviceService,
    private router: Router
  ) { }

  ngOnInit() {
    if (this.route.snapshot.paramMap.get("id") !== null) {
      this.areaData.id = this.route.snapshot.paramMap.get("id");
      this.loadData();
    }
  }

  loadData() {
    this.areasservice.get(this.areaData.id).subscribe(data => {
      this.areaData = data.data() as Areas;
    });
  }



  update() {
    this.areasservice.store(this.areaData).then(
      data => {
        this.generalservice.eventShowAlert.emit({
          text: "Curso guardada exitosamente",
          type: "success"
        });
      },
      error => { }
    );
  }

  store() {
    if (this.areaData.id) {

      this.listTestDelete.forEach(test => {
        this.banksservice.delete(test.id)
      });

      this.listTestDelete = []

      this.areasservice.update(this.areaData).then(
        () => {
          this.generalservice.eventShowAlert.emit({
            text: "Curso actualizada correctamente",
            type: "success"
          });
        },
        error => {
          this.generalservice.eventShowAlert.emit({
            text: "Error en actualización de Curso",
            type: "danger"
          });
        }
      );
    } else {
      this.areasservice.store(this.areaData).then(
        data => {
          this.generalservice.eventShowAlert.emit({
            text: "Curso guardada exitosamente",
            type: "success"
          });
        },
        error => {
          this.generalservice.eventShowAlert.emit({
            text: "Error en actualización de Curso",
            type: "danger"
          });
        }
      );
    }
  }

  deletePrueba() {
    this.modalDeletePrueba = true;
  }

  confirmDeleteArea() {
    this.areasservice.delete(this.areaData.id).then(
      () => {
        this.generalservice.eventShowAlert.emit({
          text: "Se ha eliminado el curso correctamente",
          type: "success"
        });
        this.router.navigate(['admin', 'areas']);
      },
      error => {
        this.generalservice.eventShowAlert.emit({
          text: "Ha ocurrido un error eliminando esta curso",
          type: "error"
        });
      }
    )
  }

  loadDetailTest(test: Test, index: number) {
    this.newTest = test;
    this.disableForm = true;
    this.inNewTest = false;
    this.indexTestEdit = index
  }



  formNewTest() {
    this.disableForm = true;
    this.inNewTest = true;
  }

  storeTest(test) {
    if (this.inNewTest) {
      this.areaData.test.push(test);
    } else {
      this.areaData.test[this.indexTestEdit] = test
    }
    this.cancelTest()
  }

  deleteTest() {
    this.modalConfirmDelete = true
  }

  confirmDeleteTest() {
    let index = this.areaData.test.indexOf(this.newTest)
    this.areaData.test.splice(index, 1)
    this.listTestDelete[this.listTestDelete.length] = this.newTest
    this.cancelTest()
    this.modalConfirmDelete = false
  }

  cancelTest() {
    this.disableForm = false
    this.inNewTest = false
    this.newTest = {
      name: "",
      description: "",
      maxScore: 20,
      nroQuestions: 10,
      id_bank: ''
    };
  }

  loadBank() {
    this.banksservice.get(this.newTest.id_bank).subscribe(
      data => {
        this.bankSelect = data.data() as Bancos
        this.inBank = true
        this.disableFormTest = true
        this.scrollBank()
      }
    )
  }

  saveBank() {
    this.banksservice.update(this.bankSelect).then(
      () => {
        this.generalservice.eventShowAlert.emit({
          text: "Se han guardado correctamente las preguntas",
          type: "success"
        });
      },
      error => {
        this.generalservice.eventShowAlert.emit({
          text: "Ha ocurrido un error modificando el banco de preguntas, intente nuevamente.",
          type: "error"
        });
      }
    )
  }

  closeBank() {
    this.inBank = false
    this.disableFormTest = false
  }


  scrollBank() {
    let el = document.getElementById('bankSector')
    el.scrollIntoView()
    this.bankSectorHTML.nativeElement.scrollIntoView()
  }

  addQuestion() {
    this.inNewQuestion = true
    this.questionData = {
      type: 'open',
      text: '',
      id: ''
    }
    this.modalQuestion = true
  }

  editQuestion(question, index) {
    this.questionData = question
    this.modalQuestion = true
    this.inNewQuestion = false
    this.indexQuestionEdit = index
  }

  confirmAddQuestion() {
    if (this.inNewQuestion) {
      this.bankSelect.questions.push(this.questionData)
    } else {
      this.bankSelect.questions[this.indexQuestionEdit] = this.questionData
    }
    this.cancelAddQuestion()
  }

  cancelAddQuestion() {
    this.modalQuestion = false
    this.resetDataQuestion()
  }

  resetDataQuestion() {
    this.questionData = {
      type: 'open',
      text: '',
      id: ''
    }
  }


  deleteQuestion(question) {
    this.modalConfirmDeleteQuestion = true
    this.questionData = question
  }

  cancelDeleteQuestion() {
    this.modalConfirmDeleteQuestion = false
    this.resetDataQuestion()
  }

  confirmDeleteQuestion() {
    let index = this.bankSelect.questions.indexOf(this.questionData)
    this.bankSelect.questions.splice(index, 1)
    this.modalConfirmDeleteQuestion = false
    this.resetDataQuestion()
  }

}
