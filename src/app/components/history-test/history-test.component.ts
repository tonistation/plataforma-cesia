import { Component, OnInit, Input, Output } from "@angular/core";
import { Testusuarios } from "src/app/interfaces/testusuarios";
import { PruebasService } from "src/app/services/pruebas.service";
import { GeneralserviceService } from "src/app/services/generalservice.service";

@Component({
  selector: "app-history-test",
  templateUrl: "./history-test.component.html",
  styleUrls: ["./history-test.component.scss"],
})
export class HistoryTestComponent implements OnInit {
  @Input() test: Testusuarios;
  @Input() type: string;
  @Input() section: string;

  public link: string;
  public modalRemove = false;
  public idRemove: string;

  constructor(
    private testservice: PruebasService,
    private generalservice: GeneralserviceService
  ) {}

  ngOnInit() {
    this.generateLink();
    console.log(this.test);
  }

  generateLink() {
    if (this.section === "alumn") {
      this.link = "/test/detalle/" + this.test.id;
    } else if (this.section === "teacher") {
      if (this.type === "resume") {
        if (this.test.status === 1) {
          this.link = "/profesor/test/evaluar/" + this.test.id;
        } else if (this.test.status === 2) {
          this.link = "/profesor/test/detalle/" + this.test.id;
        }
      } else if (this.type === "detail") {
        this.link = "/profesor/test/detalle/" + this.test.id;
      }
    }
  }

  openModalRemove(id: string) {
    this.idRemove = id;
    this.modalRemove = true;
  }

  removeTest() {
    this.testservice
      .delete(this.idRemove)
      .then((e) => {
        this.generalservice.eventShowAlert.emit({
          text: "Prueba eliminada correctamente",
          type: "success",
        });
        this.modalRemove = false;
        window.location.reload();
      })
      .catch(() => {
        this.generalservice.eventShowAlert.emit({
          text: "Error eliminando prrueba",
          type: "error",
        });
        this.modalRemove = false;
      });
  }
}
