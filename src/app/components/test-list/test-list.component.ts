import { Component, OnInit, Input } from "@angular/core";
import { PruebasService } from "src/app/services/pruebas.service";
import { AuthserviceService } from "src/app/services/authservice.service";
import { AreaserviceService } from "src/app/services/areaservice.service";
import { Areas } from "src/app/interfaces/areas";
import { Usuarios } from "src/app/interfaces/usuarios";
import { Areasusuarios } from "src/app/interfaces/areasusuarios";
import { Test } from "src/app/interfaces/test";
import { Testusuarios } from "src/app/interfaces/testusuarios";
import { BanksService } from "src/app/services/banks.service";
import { GeneralserviceService } from "src/app/services/generalservice.service";
import { Router, ActivatedRoute } from "@angular/router";
import { AngularFirestore } from "@angular/fire/firestore";
import { PreguntasUsuarios } from "src/app/interfaces/preguntas-usuarios";
import { Subscription } from "rxjs";
import { ClrDatagridSortOrder } from "@clr/angular";
import { TestUserComponent } from "../test-user/test-user.component";
import { Cursos } from "src/app/interfaces/cursos";
import { Cursousuario } from "src/app/interfaces/cursousuario";
import { CoursesserviceService } from "src/app/services/coursesservice.service";
import { Cursosactivos } from "src/app/interfaces/cursosactivos";

@Component({
  selector: "app-test-list",
  templateUrl: "./test-list.component.html",
  styleUrls: ["./test-list.component.scss"],
})
export class TestListComponent implements OnInit {
  @Input() curso: Cursosactivos;

  public ascSort = ClrDatagridSortOrder.ASC;

  public tabTestPending: boolean = true;
  public tabTestHistory: boolean = false;
  public inLoad: boolean = false;
  public inLoadHistory: boolean = false;
  public hayData: boolean = false;
  public hayDataHistory: boolean = false;
  public readyData = false;

  public userData: Usuarios;
  public areasTestUser: Areas[] = [];
  public cursosTestUser: Cursosactivos[] = [];
  public cursosData: Cursosactivos[] = [];
  public testHistoryData: Testusuarios[] = [];

  public modalConfirmTestStart: boolean = false;
  public areaSelData: Areas = {
    name: "",
    description: "",
    test: [],
  };

  public cursoSelData: Cursos = {
    name: "",
    description: "",
    pruebas: [],
    libros: [],
    materiales: [],
    videos: [],
  };

  public testSelData: Test = {
    name: "",
    description: "",
    maxScore: 0,
    nroQuestions: 0,
    id_bank: "",
  };
  public inGenerateTest: boolean = false;

  constructor(
    private authservice: AuthserviceService,
    private areasservice: AreaserviceService,
    private cursosservice: CoursesserviceService,
    private bankservice: BanksService,
    private pruebasservice: PruebasService,
    private generalservice: GeneralserviceService,
    private router: Router,
    private route: ActivatedRoute,
    private afs: AngularFirestore
  ) {}

  ngOnInit() {
    this.getUserData();
    this.getCursosData();
    console.log(this.curso);
    this.curso.pruebas.forEach((e) => {
      //console.log(e);
    });
  }

  getUserData() {
    //this.inLoad = true;
    this.inLoadHistory = true;
    this.userData = this.authservice.getUserData();
  }

  getCursosData() {
    let subscripcion: Subscription;
    subscripcion = this.cursosservice.listActives().subscribe(
      (data) => {
        this.cursosData = data as Cursosactivos[];
        if (this.route.snapshot.paramMap.get("tab") !== null) {
          if (this.route.snapshot.paramMap.get("tab") === "historial") {
            this.tabTestPending = false;
            this.tabTestHistory = true;
          }
        }
        this.loadHistory();
        this.unsubscribeMethod(subscripcion);
      },
      (error) => {
        this.generalservice.eventShowAlert.emit({
          text:
            "Ha ocurrido un error cargando las areas, intente nuevamente o verifique su conexion a internet.",
          type: "error",
        });
      }
    );
  }

  getTestsData() {
    this.cursosTestUser = [];
    //console.log(this.userData.type);

    if (this.userData.type == 1) {
      let dataadd = this.curso;
      //dataadd.status = -1
      this.testHistoryData.forEach((testUser) => {
        dataadd.pruebas.forEach((test) => {
          if (testUser.id_test == test.id) {
            test.status = testUser.status;
            test.id_testUser = testUser.id;
            test.dateInitialize =
              testUser.dateInitialize !== null &&
              testUser.dateInitialize !== undefined
                ? testUser.dateInitialize
                : null;
          }
        });
      });
      this.cursosTestUser.push(dataadd);
      this.inLoad = false;
    } else if (this.userData.type == 2) {
      let dataadd = this.curso;
      //dataadd.status = -1
      this.testHistoryData.forEach((testUser) => {
        dataadd.pruebas.forEach((test) => {
          if (testUser.id_test == test.id) {
            test.status = testUser.status;
            test.id_testUser = testUser.id;
            test.dateInitialize =
              testUser.dateInitialize !== null &&
              testUser.dateInitialize !== undefined
                ? testUser.dateInitialize
                : null;
          }
        });
      });
      this.cursosTestUser.push(dataadd);
      this.inLoad = false;
      this.readyData = true;
      console.log("TERMINA", this.cursosTestUser);
    }

    this.readyData = true;

    this.cursosTestUser.forEach((curso) => {
      curso.pruebas.forEach((test) => {
        if (test.status === undefined || test.status === 0) {
          this.hayData = true;
        }
      });
    });
  }

  confirmTest(test: Test) {
    this.modalConfirmTestStart = true;

    this.testSelData = test;
  }

  generateTest() {
    this.inGenerateTest = true;

    this.bankservice.get(this.testSelData.id_bank).subscribe(
      (data) => {
        let questionData = data.data().questions as PreguntasUsuarios[];
        questionData.forEach((question: PreguntasUsuarios) => {
          question.score = 0;
          question.answer = "";
        });

        if (questionData.length < this.testSelData.nroQuestions) {
          this.generalservice.eventShowAlert.emit({
            text: "Esta prueba no puede ser realizada en estos momentos.",
            type: "info",
          });
          this.modalConfirmTestStart = false;
          this.inGenerateTest = false;
        } else {
          let arrayQuestionsRamdon = this.getRandom(
            questionData,
            this.testSelData.nroQuestions
          );
          let testusuarios: Testusuarios = {
            name: this.testSelData.name,
            id_test: this.testSelData.id,
            id_area: this.curso.id_curso,
            id_curso: this.curso.id,
            id_user: this.userData.id,
            score: 0,
            comments: "",
            questions: arrayQuestionsRamdon,
            status: 0,
            dateInitialize: new Date(),
            dateEvaluate: null,
            dateRealize: null,
          };

          this.saveTestUser(testusuarios);
        }
      },
      (error) => {
        this.generalservice.eventShowAlert.emit({
          text: "Ha ocurrido un error, intente nuevamente",
          type: "error",
        });
      }
    );
  }

  saveTestUser(data) {
    let idTest = this.afs.createId();
    data.id = idTest;

    this.pruebasservice.store(data).then(
      () => {
        this.generalservice.eventShowAlert.emit({
          text: "Prueba generada exitosamente",
          type: "success",
        });
        this.router.navigate(["test", "realizar", idTest]);
      },
      (error) => {
        this.generalservice.eventShowAlert.emit({
          text: "Error genrando la prueba",
          type: "error",
        });
      }
    );
  }

  getRandom(arr, n) {
    var result = new Array(n),
      len = arr.length,
      taken = new Array(len);
    if (n > len)
      throw new RangeError("getRandom: more elements taken than available");
    while (n--) {
      var x = Math.floor(Math.random() * len);
      result[n] = arr[x in taken ? taken[x] : x];
      taken[x] = --len in taken ? taken[len] : len;
    }
    return result;
  }

  loadHistory() {
    this.inLoadHistory = true;
    this.hayDataHistory = false;
    let subscripcion: Subscription;
    subscripcion = this.pruebasservice.getHistory(this.userData.id).subscribe(
      (data) => {
        this.testHistoryData = data as Testusuarios[];
        this.cursosData.forEach((area) => {
          this.testHistoryData.forEach((testUser) => {
            // console.log(testUser, area);

            if (testUser.id_curso === area.id) {
              testUser.name_area = area.name;
            }
            if (testUser.status === 1 || testUser.status === 2) {
              this.hayDataHistory = true;
            }
            testUser.dateInitialize = this.generalservice.convertDate(
              testUser.dateInitialize
            );
            testUser.dateEvaluate = this.generalservice.convertDate(
              testUser.dateEvaluate
            );
            testUser.dateRealize = this.generalservice.convertDate(
              testUser.dateRealize
            );
          });
        });
        this.getTestsData();
        //console.log(this.testHistoryData)
        this.unsubscribeMethod(subscripcion);
        this.inLoadHistory = false;
      },
      (error) => {
        this.generalservice.eventShowAlert.emit({
          text: "Error obteniendo historial de pruebas",
          type: "error",
        });
        this.inLoadHistory = false;
      }
    );
  }

  unsubscribeMethod(suscripcion: Subscription) {
    suscripcion.unsubscribe();
  }
}
