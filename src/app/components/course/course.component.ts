import { Component, OnInit, ViewChild, ElementRef } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { GeneralserviceService } from "src/app/services/generalservice.service";
import { Cursos } from "src/app/interfaces/cursos";
import { Libros } from "src/app/interfaces/libros";
import { CoursesserviceService } from "src/app/services/coursesservice.service";
import { AngularFirestore } from "@angular/fire/firestore";
import { Videos } from "src/app/interfaces/videos";
import { Materiales } from "src/app/interfaces/materiales";
import {
  AngularFireStorage,
  AngularFireStorageReference,
} from "@angular/fire/storage";
import { DomSanitizer, SafeResourceUrl } from "@angular/platform-browser";
import * as ClassicEditor from "@ckeditor/ckeditor5-build-classic";
import { Bancos } from "src/app/interfaces/bancos";
import { BanksService } from "src/app/services/banks.service";
import { Test } from "src/app/interfaces/test";
import { Preguntas } from "src/app/interfaces/preguntas";

declare var Yumpu: any;

@Component({
  selector: "app-course",
  templateUrl: "./course.component.html",
  styleUrls: ["./course.component.scss"],
})
export class CourseComponent implements OnInit {
  @ViewChild("bankSector", { static: false }) bankSectorHTML: ElementRef;
  @ViewChild("sectorTop", { static: false }) sectorTop: ElementRef;

  public configEditor = { language: "es" };

  public cursoData: Cursos = {
    id: "",
    name: "",
    description: "",
    libros: [],
    materiales: [],
    videos: [],
    pruebas: [],
  };

  public newLibro: Libros = {
    name: "",
    id: undefined,
    description: "",
    id_embed: "",
    type: "",
  };

  public newVideo: Videos = {
    name: "",
    id: undefined,
    description: "",
    url: "",
  };

  public newMaterial: Materiales = {
    name: "",
    id: undefined,
    url: "",
    type: "",
  };

  public newTest: Test = {
    name: "",
    description: "",
    maxScore: 20,
    nroQuestions: 10,
    id_bank: "",
  };

  public bankSelect: Bancos = {
    questions: [],
  };

  public questionData: Preguntas = {
    type: "open",
    text: "",
    id: "",
  };

  public listTestDelete: Test[] = new Array();

  public disableForm: boolean = false;
  public disableFormLibro: boolean = false;
  public inNewLibro: boolean = false;
  public inNewVideo: boolean = false;
  public inNewMaterial: boolean = false;
  private indexEdit: number;
  public inEdit: "" | "libro" | "video" | "material" | "prueba";
  public modalDeleteCurso: boolean = false;
  private ref: AngularFireStorageReference;
  public videoUrlString: SafeResourceUrl;

  public disableFormTest: boolean = false;
  public inNewTest: boolean = false;
  public inNewQuestion: boolean = false;
  private indexTestEdit: number;
  private indexQuestionEdit: number;
  public modalDeletePrueba: boolean = false;
  public modalConfirmDelete: boolean = false;
  public modalQuestion: boolean = false;
  public modalConfirmDeleteQuestion: boolean = false;
  public inBank: boolean = false;
  public Editor = ClassicEditor;

  constructor(
    private route: ActivatedRoute,
    private coursesservice: CoursesserviceService,
    private generalservice: GeneralserviceService,
    private banksservice: BanksService,
    private router: Router,
    private afs: AngularFirestore,
    public sanitizer: DomSanitizer,
    private afStorage: AngularFireStorage
  ) {}

  ngOnInit() {
    if (this.route.snapshot.paramMap.get("id") !== null) {
      this.cursoData.id = this.route.snapshot.paramMap.get("id");
      this.loadData();
    }
  }

  loadData() {
    this.coursesservice.get(this.cursoData.id).subscribe((data) => {
      this.cursoData = data.data() as Cursos;
    });
  }

  update() {
    this.coursesservice.store(this.cursoData).then(
      (data) => {
        this.generalservice.eventShowAlert.emit({
          text: "Curso guardada exitosamente",
          type: "success",
        });
      },
      (error) => {}
    );
  }

  store() {
    if (this.cursoData.id) {
      this.listTestDelete.forEach((test) => {
        this.banksservice.delete(test.id);
      });

      this.listTestDelete = [];

      this.coursesservice.update(this.cursoData).then(
        () => {
          this.generalservice.eventShowAlert.emit({
            text: "Curso actualizada correctamente",
            type: "success",
          });
        },
        (error) => {
          this.generalservice.eventShowAlert.emit({
            text: "Error en actualización de Curso",
            type: "danger",
          });
        }
      );
    } else {
      console.log(this.cursoData);
      this.cursoData.id = this.afs.createId();

      this.coursesservice.store(this.cursoData).then(
        (data) => {
          this.generalservice.eventShowAlert.emit({
            text: "Curso guardada exitosamente",
            type: "success",
          });
        },
        (error) => {
          this.generalservice.eventShowAlert.emit({
            text: "Error en actualización de Curso",
            type: "danger",
          });
        }
      );
    }
  }

  deleteCurso() {
    this.modalDeleteCurso = true;
  }

  confirmDeleteCurso() {
    this.coursesservice.delete(this.cursoData.id).then(
      () => {
        this.generalservice.eventShowAlert.emit({
          text: "Se ha eliminado el curso correctamente",
          type: "success",
        });
        this.router.navigate(["admin", "areas"]);
      },
      (error) => {
        this.generalservice.eventShowAlert.emit({
          text: "Ha ocurrido un error eliminando esta curso",
          type: "error",
        });
      }
    );
  }

  loadDetailLibro(book: Libros, index: number) {
    this.newLibro = book;
    this.disableForm = true;
    this.inNewLibro = false;
    this.indexEdit = index;
    this.inEdit = "libro";
    this.setBook(book);
  }

  setBook(libro: Libros) {
    if (libro !== null) {
      //console.log(Yumpu());
      var myYumpu = new Yumpu();
      myYumpu.create_player("#yumpuMagazineContainer", "", {
        lang: "es",
        embed_id: libro.id_embed,
        embedded: true,
      });
    } else {
      if (document.getElementById("yumpuMagazineContainer") !== null)
        document.getElementById("yumpuMagazineContainer").innerHTML = "";
    }
  }

  formNewLibro() {
    this.disableForm = true;
    this.inNewLibro = true;
    this.inEdit = "libro";
    this.setBook(null);
  }

  storeLibro(book) {
    if (this.cursoData.libros == undefined) {
      this.cursoData.libros = [];
    }
    if (this.inNewLibro) {
      this.cursoData.libros.push(book);
    } else {
      this.cursoData.libros[this.indexEdit] = book;
    }
    if (!book.id || book.id === undefined) {
      book.id = this.afs.createId();
    }
    this.storePropertyDB("libros");
    this.cancelItem();
  }

  deleteLibro() {
    let index = this.cursoData.libros.indexOf(this.newLibro);
    this.cursoData.libros.splice(index, 1);
    this.storePropertyDB("libros");
    this.cancelItem();
  }

  cancelItem() {
    this.disableForm = false;
    this.inNewLibro = false;
    this.inNewVideo = false;
    this.inNewMaterial = false;
    this.newLibro = {
      name: "",
      description: "",
      id_embed: "",
      type: "",
    };
    this.newVideo = {
      name: "",
      description: "",
      id: undefined,
      url: "",
    };

    this.newMaterial = {
      name: "",
      id: undefined,
      url: "",
      type: "",
    };
    this.inEdit = "";
  }

  loadDetailVideo(video: Videos, index: number) {
    this.newVideo = video;
    this.disableForm = true;
    this.inNewVideo = false;
    this.indexEdit = index;
    this.inEdit = "video";
    let url: string = video.url as string;
    this.setVideo(url);
  }

  setVideo(url: string) {
    this.newVideo.url = url;
    this.videoUrlString = this.sanitizer.bypassSecurityTrustResourceUrl(
      "https://www.youtube-nocookie.com/embed/" + this.newVideo.url
    );
  }

  formNewVideo() {
    this.disableForm = true;
    this.inNewVideo = true;
    this.inEdit = "video";
    this.setVideo("");
  }

  storeVideo(video) {
    if (this.cursoData.videos == undefined) {
      this.cursoData.videos = [];
    }
    if (this.inNewVideo) {
      video.id = this.afs.createId();
      this.cursoData.videos.push(video);
    } else {
      this.cursoData.videos[this.indexEdit] = video;
    }
    if (!video.id || video.id === undefined) {
      video.id = this.afs.createId();
    }
    this.storePropertyDB("videos");
    this.cancelItem();
  }

  deleteVideo() {
    let index = this.cursoData.videos.indexOf(this.newVideo);
    this.cursoData.videos.splice(index, 1);
    this.storePropertyDB("videos");
    this.cancelItem();
  }

  loadDetailMaterial(material: Materiales, index: number) {
    this.newMaterial = material;
    this.disableForm = true;
    this.inNewMaterial = false;
    this.indexEdit = index;
    this.inEdit = "material";
  }

  formNewMaterial() {
    this.disableForm = true;
    this.inNewMaterial = true;
    this.inEdit = "material";
  }

  storeMaterial(material) {
    if (this.cursoData.materiales == undefined) {
      this.cursoData.materiales = [];
    }

    if (this.inNewMaterial) {
      material.id = this.afs.createId();
      this.cursoData.materiales.push(material);
    } else {
      this.cursoData.materiales[this.indexEdit] = material;
    }
    if (!material.id || material.id === undefined) {
      material.id = this.afs.createId();
    }
    this.storePropertyDB("materiales");
    this.cancelItem();
  }

  deleteMaterial() {
    let index = this.cursoData.materiales.indexOf(this.newMaterial);
    this.cursoData.materiales.splice(index, 1);
    this.storePropertyDB("materiales");
    this.cancelItem();
  }

  upload(event) {
    this.generalservice.eventShowAlert.emit({
      text: "Cargando archivo",
      type: "info",
    });
    const randomId = Math.random().toString(36).substring(2);
    const filename =
      "/" +
      this.cursoData.id +
      "/" +
      randomId +
      "_" +
      event.target.files[0].name;

    const upload = this.afStorage
      .upload(filename, event.target.files[0])
      .then(() => {
        const ref = this.afStorage.ref(filename);
        const downloadURL = ref.getDownloadURL().subscribe((url) => {
          this.newMaterial.url = url;
          this.generalservice.eventShowAlert.emit({
            text: "Archivo Subido",
            type: "success",
          });
        });
      });
  }

  deletePrueba() {
    this.modalDeletePrueba = true;
  }
  loadDetailTest(test: Test, index: number) {
    this.newTest = test;
    this.disableForm = true;
    this.inNewTest = false;
    this.indexTestEdit = index;
    this.inEdit = "prueba";
    this.sectorTop.nativeElement.scrollIntoView();
  }

  formNewTest() {
    this.disableForm = true;
    this.inNewTest = true;
    this.inEdit = "prueba";
    this.sectorTop.nativeElement.scrollIntoView();
  }

  storeTest(test) {
    if (this.cursoData.pruebas == undefined) {
      this.cursoData.pruebas = [];
    }

    if (this.inNewTest) {
      test.id = this.afs.createId();
      this.cursoData.pruebas.push(test);
    } else {
      this.cursoData.pruebas[this.indexTestEdit] = test;
    }
    if (!test.id || test.id === undefined) {
      test.id = this.afs.createId();
    }
    this.storePropertyDB("pruebas");
    this.cancelTest();
  }

  deleteTest() {
    this.modalConfirmDelete = true;
  }

  confirmDeleteTest() {
    let index = this.cursoData.pruebas.indexOf(this.newTest);
    this.cursoData.pruebas.splice(index, 1);
    this.listTestDelete[this.listTestDelete.length] = this.newTest;
    this.storePropertyDB("pruebas");
    this.cancelTest();
    this.modalConfirmDelete = false;
  }

  cancelTest() {
    this.disableForm = false;
    this.inNewTest = false;
    this.newTest = {
      name: "",
      description: "",
      maxScore: 20,
      nroQuestions: 10,
      id_bank: "",
    };
  }

  loadBank() {
    this.banksservice.get(this.newTest.id_bank).subscribe((data) => {
      this.bankSelect = data.data() as Bancos;
      this.inBank = true;
      this.disableFormTest = true;
      this.scrollBank();
    });
  }

  saveBank() {
    this.banksservice.update(this.bankSelect).then(
      () => {
        this.generalservice.eventShowAlert.emit({
          text: "Se han guardado correctamente las preguntas",
          type: "success",
        });
      },
      (error) => {
        this.generalservice.eventShowAlert.emit({
          text:
            "Ha ocurrido un error modificando el banco de preguntas, intente nuevamente.",
          type: "error",
        });
      }
    );
  }

  closeBank() {
    this.inBank = false;
    this.disableFormTest = false;
    this.sectorTop.nativeElement.scrollIntoView();
  }

  scrollBank() {
    let el = document.getElementById("bankSector");
    el.scrollIntoView();
    this.bankSectorHTML.nativeElement.scrollIntoView();
  }

  addQuestion() {
    this.inNewQuestion = true;
    this.questionData = {
      type: "open",
      text: "",
      id: "",
    };
    this.modalQuestion = true;
  }

  editQuestion(question, index) {
    this.questionData = question;
    this.modalQuestion = true;
    this.inNewQuestion = false;
    this.indexQuestionEdit = index;
  }

  confirmAddQuestion() {
    if (this.inNewQuestion) {
      this.bankSelect.questions.push(this.questionData);
    } else {
      this.bankSelect.questions[this.indexQuestionEdit] = this.questionData;
    }
    this.cancelAddQuestion();
  }

  cancelAddQuestion() {
    this.modalQuestion = false;
    this.resetDataQuestion();
  }

  resetDataQuestion() {
    this.questionData = {
      type: "open",
      text: "",
      id: "",
    };
  }

  deleteQuestion(question) {
    this.modalConfirmDeleteQuestion = true;
    this.questionData = question;
  }

  cancelDeleteQuestion() {
    this.modalConfirmDeleteQuestion = false;
    this.resetDataQuestion();
  }

  confirmDeleteQuestion() {
    let index = this.bankSelect.questions.indexOf(this.questionData);
    this.bankSelect.questions.splice(index, 1);
    this.modalConfirmDeleteQuestion = false;
    this.resetDataQuestion();
  }

  noCMVideo(e) {
    e.preventDefault();
    return false;
  }

  storePropertyDB(type: string) {
    const data = { [type]: this.cursoData[type] };
    this.coursesservice.storeInCurso(this.cursoData.id, data).then((data) => {
      this.generalservice.eventShowAlert.emit({
        text: "Cambios guardados correctamente",
        type: "success",
      });
    });
  }
}
