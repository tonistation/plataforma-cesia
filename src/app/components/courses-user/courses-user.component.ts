import { Component, OnInit } from "@angular/core";
import { CoursesserviceService } from "src/app/services/coursesservice.service";
import { Cursos } from "src/app/interfaces/cursos";
import { Usuarios } from "src/app/interfaces/usuarios";
import { AuthserviceService } from "src/app/services/authservice.service";
import { Cursousuario } from "src/app/interfaces/cursousuario";
import * as _ from "lodash";
import { Cursosactivos } from "src/app/interfaces/cursosactivos";
import { Subscription } from "rxjs";
import { UsersService } from "src/app/services/users.service";
import { GeneralserviceService } from "src/app/services/generalservice.service";

@Component({
  selector: "app-courses-user",
  templateUrl: "./courses-user.component.html",
  styleUrls: ["./courses-user.component.scss"],
})
export class CoursesUserComponent implements OnInit {
  public cursosData: Cursos[];
  public cursosUsuario: Cursousuario[] = [];
  public activesData: Cursosactivos[];
  constructor(
    private coursesservice: CoursesserviceService,
    private authservice: AuthserviceService,
    private usersservice: UsersService,
    private generalservice: GeneralserviceService
  ) {}
  public inLoad: boolean = false;
  public userData: Usuarios;

  ngOnInit() {
    this.getUserData();
    this.list();
  }

  getCoursesData() {
    this.inLoad = true;
    this.coursesservice.list().subscribe((data) => {
      this.cursosData = data as Cursos[];

      this.cursosData.forEach((curso) => {
        //console.log(area)
        let tempCurso: Cursousuario = {
          id_curso: curso.id,
          name_curso: curso.name,
          libros: curso.libros,
          videos: curso.videos,
          materiales: curso.materiales,
        };

        this.cursosUsuario.push(tempCurso);
      });
      this.inLoad = false;

      var temp = this.cursosUsuario;
      this.cursosUsuario = _.orderBy(temp, ["access"], ["desc"]);
    });
  }

  getUserData() {
    this.inLoad = true;
    this.userData = this.authservice.getUserData();
    this.getCoursesData();
  }

  list() {
    this.activesData = [];
    let dataFirst = [];
    this.inLoad = true;
    let subscripcion: Subscription;
    subscripcion = this.coursesservice.getByValues("status", 1).subscribe(
      (data) => {
        let thedata = data as Cursosactivos[];

        thedata.forEach((curso) => {
          let access = false;
          curso.alumnos.forEach((alumno) => {
            if (alumno.id == this.userData.id) {
              access = true;
            }
          });
          let tempCurso: Partial<Cursosactivos> = {
            id_curso: curso.id_curso,
            name: curso.name,
            id: curso.id,
            access: access,
          };
          dataFirst.push(tempCurso);
        });
        this.activesData = dataFirst;
        this.activesData.sort(this.compareSortAccess);

        this.inLoad = false;
        this.unsubscribeMethod(subscripcion);
      },
      (error) => {
        this.inLoad = false;
        this.generalservice.eventShowAlert.emit({
          text:
            "Error en la carga de usuarios, verifique la conexion a internet",
          type: "error",
        });
      }
    );
  }

  compareSortInit(a, b) {
    if (a.name < b.name) {
      return -1;
    }
    if (a.name > b.name) {
      return 1;
    }
    return 0;
  }

  compareSortAccess(a, b) {
    if (a.access > b.access) {
      return -1;
    }
    if (a.access < b.access) {
      return 1;
    }
    return 0;
  }

  unsubscribeMethod(suscripcion: Subscription) {
    suscripcion.unsubscribe();
  }
}
