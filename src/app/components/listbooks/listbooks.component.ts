import { Component, OnInit } from "@angular/core";
import { GeneralserviceService } from "src/app/services/generalservice.service";
import { ClrDatagridSortOrder } from "@clr/angular";
import { BooksService } from "src/app/services/books.service";
import { Libros } from "src/app/interfaces/libros";

@Component({
  selector: "app-listbooks",
  templateUrl: "./listbooks.component.html",
  styleUrls: ["./listbooks.component.scss"],
})
export class ListbooksComponent implements OnInit {
  public inLoad: boolean = false;
  public booksData: Libros[];
  public descSort = ClrDatagridSortOrder.DESC;

  constructor(
    private booksservice: BooksService,
    private generalservice: GeneralserviceService
  ) {}

  ngOnInit() {
    //this.descSort = ClrDatagridSortOrder.ASC;
    this.list();
  }

  list() {
    this.inLoad = true;
    this.booksservice.list().subscribe(
      (data) => {
        this.booksData = data as Libros[];

        this.booksData.sort(this.compareSortInit);
        this.inLoad = false;
      },
      (error) => {
        this.inLoad = false;
        this.generalservice.eventShowAlert.emit({
          text:
            "Error en la carga de usuarios, verifique la conexion a internet",
          type: "error",
        });
      }
    );
  }

  compareSortInit(a, b) {
    if (a.firstName < b.firstName) {
      return -1;
    }
    if (a.firstName > b.firstName) {
      return 1;
    }
    return 0;
  }
}
