import { Component, OnInit } from "@angular/core";
import { Libros } from "src/app/interfaces/libros";
import { Videos } from "src/app/interfaces/videos";
import { Materiales } from "src/app/interfaces/materiales";
import { Cursos } from "src/app/interfaces/cursos";
import { ActivatedRoute } from "@angular/router";
import { CoursesserviceService } from "src/app/services/coursesservice.service";
import { DomSanitizer, SafeResourceUrl } from "@angular/platform-browser";
import { Cursosactivos } from "src/app/interfaces/cursosactivos";
import { log } from "util";
declare var Yumpu: any;

@Component({
  selector: "app-coursesview",
  templateUrl: "./coursesview.component.html",
  styleUrls: ["./coursesview.component.scss"],
})
export class CoursesviewComponent implements OnInit {
  public tabBooks: boolean = true;
  public tabVideos: boolean = false;
  public tabMaterials: boolean = false;
  public tabPruebas: boolean = false;
  public inLoad = false;
  public bookRadio: Libros;
  public inSelectBook = false;
  public videoRadio: Partial<Videos> = {};
  public inSelectVideo = false;
  public stringVideo: SafeResourceUrl;

  public courseData: Partial<Cursos> = {
    name: "",
    description: "",
    libros: [],
    materiales: [],
    videos: [],
    pruebas: [],
  };

  constructor(
    private route: ActivatedRoute,
    private coursesservice: CoursesserviceService,
    public sanitizer: DomSanitizer
  ) {}

  ngOnInit() {
    if (this.route.snapshot.paramMap.get("code") !== null) {
      this.courseData.id = this.route.snapshot.paramMap.get("code");
      this.loadData();
    }
  }

  loadData() {
    this.inLoad = true;
    this.coursesservice
      .getActive(this.courseData.id)
      .subscribe((dataActive) => {
        let dataTemp = dataActive.data() as Cursosactivos;

        this.coursesservice.get(dataTemp.id_curso).subscribe((data) => {
          let dataFinalTemp = data.data() as Cursos;
          dataFinalTemp.id_curso = dataTemp.id;
          dataFinalTemp.name = dataTemp.name;
          if (dataFinalTemp.libros === undefined) {
            dataFinalTemp.libros = [];
          } else {
            let contenedor = [];

            dataTemp.libros.forEach((elem) => {
              if (
                dataFinalTemp.libros.some((el) => el.id == elem.id) &&
                elem.active
              ) {
                let elemento = dataFinalTemp.libros.filter(
                  (el) => el.id == elem.id
                );
                contenedor.push(elemento[0]);
              }
            });
            dataFinalTemp.libros = contenedor;
          }
          if (dataFinalTemp.libros.length == 1) {
            this.bookRadio = dataFinalTemp.libros[0];
            this.inSelectBook = false;
            this.setBook(dataFinalTemp.libros[0]);
          } else {
            this.inSelectBook = true;
          }

          if (dataFinalTemp.videos === undefined) {
            dataFinalTemp.videos = [];
          } else {
            let contenedor = [];
            dataTemp.videos.forEach((elem) => {
              if (
                dataFinalTemp.videos.some((el) => el.id == elem.id) &&
                elem.active
              ) {
                let elemento = dataFinalTemp.videos.filter(
                  (el) => el.id == elem.id
                );
                contenedor.push(elemento[0]);
              }
            });
            dataFinalTemp.videos = contenedor;
          }

          if (dataFinalTemp.videos.length == 1) {
            this.videoRadio = dataFinalTemp.videos[0];
            this.inSelectVideo = false;
            this.setVideo(dataFinalTemp.videos[0]);
          } else {
            this.inSelectVideo = true;
          }

          if (dataFinalTemp.materiales === undefined) {
            dataFinalTemp.materiales = [];
          } else {
            let contenedor = [];
            dataTemp.materiales.forEach((elem) => {
              if (
                dataFinalTemp.materiales.some((el) => el.id == elem.id) &&
                elem.active
              ) {
                let elemento = dataFinalTemp.materiales.filter(
                  (el) => el.id == elem.id
                );
                contenedor.push(elemento[0]);
              }
            });
            dataFinalTemp.materiales = contenedor;
          }

          if (dataFinalTemp.pruebas === undefined) {
            dataFinalTemp.pruebas = [];
          } else {
            let contenedor = [];
            dataTemp.pruebas.forEach((elem) => {
              if (
                dataFinalTemp.pruebas.some((el) => el.id == elem.id) &&
                elem.active
              ) {
                let elemento = dataFinalTemp.pruebas.filter(
                  (el) => el.id == elem.id
                );
                contenedor.push(elemento[0]);
              }
            });
            dataFinalTemp.pruebas = contenedor;
          }

          console.log("ASIGNAAAAAAAAAAAAAAAAAA", dataFinalTemp.pruebas);

          this.courseData = dataFinalTemp;
          this.inLoad = false;
        });
      });
  }

  reactiveBook() {
    this.tabBooks = true;
    if (this.courseData.libros.length == 1) {
      this.bookRadio = this.courseData.libros[0];
      this.inSelectBook = false;
      this.setBook(this.courseData.libros[0]);
    } else {
      this.inSelectBook = true;
    }
  }

  setBook(libro: Libros) {
    var myYumpu = new Yumpu();
    myYumpu.create_player("#yumpuMagazineContainer", "", {
      lang: "es",
      embed_id: libro.id_embed,
      embedded: true,
    });
    this.inSelectBook = false;
  }

  setVideo(video: Videos) {
    const urlTemp: string = video.url as string;

    this.stringVideo = this.sanitizer.bypassSecurityTrustResourceUrl(
      "https://www.youtube-nocookie.com/embed/" + urlTemp
    );
    this.inSelectVideo = false;
  }

  noCMVideo(e) {
    e.preventDefault();
    return false;
  }
}
