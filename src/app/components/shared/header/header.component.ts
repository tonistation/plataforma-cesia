import { Component, OnInit } from "@angular/core";
import { AuthserviceService } from "src/app/services/authservice.service";
import { Usuarios } from "src/app/interfaces/usuarios";
import { environment } from "src/environments/environment.prod";

@Component({
  selector: "app-header",
  templateUrl: "./header.component.html",
  styleUrls: ["./header.component.scss"],
})
export class HeaderComponent implements OnInit {
  public userLoad: boolean = false;
  public name_company: string;
  public userData: Usuarios = {
    identity: "",
    identityType: "",
    email: "",
    phone: "",
    firstName: "",
    lastName: "",
    areas: [],
  };

  constructor(public authservice: AuthserviceService) {}

  ngOnInit() {
    this.getUserData();
    this.name_company = environment.name_company;
  }

  getUserData() {
    setTimeout(() => {
      this.userLoad = true;
      this.userData = this.authservice.getUserData();
    }, 1000);
  }

  reloadFull() {
    window.location.reload(true);
  }
}
