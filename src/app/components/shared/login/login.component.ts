import { Component, OnInit, NgZone } from "@angular/core";
import { GeneralserviceService } from "src/app/services/generalservice.service";
import { AuthserviceService } from "src/app/services/authservice.service";
import { Router } from "@angular/router";
import { environment } from "src/environments/environment.prod";
import { UsersService } from "src/app/services/users.service";

@Component({
  selector: "app-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.scss"],
})
export class LoginComponent implements OnInit {
  public urlImg: string;
  public infoImg: string;
  public showError: boolean = false;
  public showWelcome: boolean = false;
  public form: { email: ""; password: "" };
  public name_company: string;
  public errorMsg: string = "Error, verifique sus datos de acceso!";

  constructor(
    private generalService: GeneralserviceService,
    private authservice: AuthserviceService,
    private usersservice: UsersService,
    public router: Router,
    public ngZone: NgZone
  ) {}

  ngOnInit() {
    //this.getImgDay();
    this.urlImg = "https://i.ibb.co/Vvc8hBQ/fondologin1.jpg";
    this.name_company = environment.name_company;
  }

  getImgDay() {
    this.generalService.getPicOfDay().subscribe((data: any) => {
      this.urlImg = "https://www.bing.com/" + data.images[0].url;
      this.infoImg = data.images[0].copyright;
    });
  }

  logIn(email: string, password: string) {
    this.authservice
      .LogIn(email, password)
      .then((result) => {
        console.log(result);

        this.showWelcome = true;
      })
      .catch((error) => {
        this.errorMsg = "Error, verifique sus datos de acceso!";
        this.showError = true;
      });
  }

  prepareLogin(email: string, password: string) {
    password = email;
    this.showError = false;
    if (email.trim() !== "" && password.trim() !== "") {
      this.usersservice.getBy("identity", email.trim()).subscribe(
        (result) => {
          console.log(result);
          console.log(result.docs[0].data());
          if (result.docs.length > 0) {
            if (!result.docs[0].data().active) {
              this.errorMsg =
                "Este usuario se encuentra desactivado temporalmente";
              this.showError = true;
            } else if (result.docs[0].data().type !== 2) {
              this.errorMsg =
                "Solo pueden acceder alumnos desde esta pantalla.";
              this.showError = true;
            } else {
              this.logIn(email, password);
            }
          } else {
            this.logIn(email, password);
          }
        },
        (error) => {
          this.errorMsg = "Error verificando datos";
          this.showError = true;
        }
      );
    } else {
      this.errorMsg = "Debe introducir correo y clave para acceder";
      this.showError = true;
    }
  }
}
