import { Component, OnInit } from "@angular/core";
import { BooksService } from "src/app/services/books.service";
import { Libros } from "src/app/interfaces/libros";

@Component({
  selector: "app-book",
  templateUrl: "./book.component.html",
  styleUrls: ["./book.component.scss"]
})
export class BookComponent implements OnInit {
  public booksData: Libros[];
  constructor(private bookservice: BooksService) {}

  ngOnInit() {
    this.load();
  }

  load() {
    this.bookservice.list().subscribe(data => {
      this.booksData = data as Libros[];
    });
  }
}
