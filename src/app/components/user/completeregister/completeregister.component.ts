import { Component, OnInit } from "@angular/core";
import { GeneralserviceService } from "src/app/services/generalservice.service";
import { AuthserviceService } from "src/app/services/authservice.service";

@Component({
  selector: "app-completeregister",
  templateUrl: "./completeregister.component.html",
  styleUrls: ["./completeregister.component.scss"],
})
export class CompleteregisterComponent implements OnInit {
  public urlImg: string;
  public infoImg: string;
  public dataRegister = {
    nroBoleta: "",
    password: "",
    email: "",
    passwordVerify: "",
    firstName: "",
    lastName: "",
    identity: "",
  };
  public showError = {
    nroBoleta: false,
    password: false,
  };
  public stepRegister = 1;

  constructor(
    private generalService: GeneralserviceService,
    private authservice: AuthserviceService
  ) {}

  ngOnInit() {
    //this.getImgDay();
    this.urlImg = "https://i.ibb.co/Vvc8hBQ/fondologin1.jpg";
  }

  verificarDocumento() {
    this.authservice.checkDocumento(this.dataRegister.nroBoleta).subscribe(
      (result) => {
        console.log(result.docs[0].data());
        if (result.docs.length > 0) {
          this.authservice
            .getUser(result.docs[0].data().id)
            .subscribe((resultUser) => {
              console.log(resultUser);

              if (resultUser.data() !== undefined) {
                this.dataRegister.identity = resultUser.data().identity;
                this.dataRegister.firstName = resultUser.data().firstName;
                this.dataRegister.lastName = resultUser.data().lastName;
                this.stepRegister = 2;
                this.dataRegister.email =
                  result.docs[0].data().identity + "@esconfa.com";
                this.stepRegister = 2;
              } else {
                this.generalService.eventShowAlert.emit({
                  text: "Error procesando el numero de boleta",
                  type: "error",
                });
              }
            });
        } else {
          this.authservice
            .checkRegisterCode(this.dataRegister.nroBoleta)
            .subscribe(
              (result) => {
                //console.log(result.docs[0].data())
                if (result.docs.length > 0) {
                  this.dataRegister.identity = result.docs[0].data().identity;
                  this.dataRegister.identity = result.docs[0].data().identity;
                  this.dataRegister.firstName = result.docs[0].data().firstName;
                  this.dataRegister.email =
                    result.docs[0].data().identity + "@esconfa.com";
                  this.stepRegister = 2;
                }
              },
              (error) => {
                this.generalService.eventShowAlert.emit({
                  text: "Error verificando datos",
                  type: "error",
                });
              }
            );
          this.generalService.eventShowAlert.emit({
            text: "El numero de boleta no se encuentra registrado",
            type: "error",
          });
        }
      },
      (error) => {
        console.log(error);
      }
    );
  }

  createPassword() {
    if (
      this.dataRegister.password !== "" &&
      this.dataRegister.passwordVerify !== ""
    ) {
      if (
        this.dataRegister.password == this.dataRegister.passwordVerify &&
        this.dataRegister.password.length > 5
      ) {
        console.log(this.dataRegister);
        this.authservice.Register(
          this.dataRegister.email,
          this.dataRegister.password
        );
        this.stepRegister = 3;
      } else {
        this.showError.password = true;
      }
    } else {
      this.showError.password = true;
    }
  }

  getImgDay() {
    this.generalService.getPicOfDay().subscribe((data: any) => {
      this.urlImg = "https://www.bing.com/" + data.images[0].url;
      this.infoImg = data.images[0].copyright;
    });
  }
}
