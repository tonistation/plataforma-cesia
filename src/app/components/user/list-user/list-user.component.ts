import { Component, OnInit } from '@angular/core';
import { UsersService } from 'src/app/services/users.service';
import { Usuarios } from 'src/app/interfaces/usuarios';
import { GeneralserviceService } from 'src/app/services/generalservice.service';
import { ClrDatagridSortOrder } from '@clr/angular';

@Component({
  selector: 'app-list-user',
  templateUrl: './list-user.component.html',
  styleUrls: ['./list-user.component.scss']
})
export class ListUserComponent implements OnInit {
  public inLoad: boolean = false
  public listData: Usuarios[]
  public descSort = ClrDatagridSortOrder.DESC

  constructor(
    private userservice: UsersService,
    private generalservice: GeneralserviceService
  ) { }

  ngOnInit() {
    //this.descSort = ClrDatagridSortOrder.ASC;
    this.list()
  }

  list() {
    this.inLoad = true
    this.userservice.list().subscribe(
      data => {
        this.listData = data as Usuarios[]
        this.listData.sort(this.compareSortInit)
        this.inLoad = false
      },
      error => {
        this.inLoad = false
        this.generalservice.eventShowAlert.emit({ text: 'Error en la carga de usuarios, verifique la conexion a internet', type: 'error' })
      }
    )
  }

  compareSortInit(a, b) {
    if (a.firstName < b.firstName) {
      return -1;
    }
    if (a.firstName > b.firstName) {
      return 1;
    }
    return 0;
  }

  enableuser(usuario: Usuarios) {
    this.userservice.setactive(usuario).then(
      () => {
        this.generalservice.eventShowAlert.emit({ text: 'Usuario Inactivo', type: 'success' })
      }
    )
  }

}