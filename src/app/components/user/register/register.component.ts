import { Component, OnInit } from "@angular/core";
import { Usuarios } from "src/app/interfaces/usuarios";
import { AreaserviceService } from "src/app/services/areaservice.service";
import { Areas } from "src/app/interfaces/areas";
import { Pago } from "src/app/interfaces/pago";
import { UsersService } from "src/app/services/users.service";
import { GeneralserviceService } from "src/app/services/generalservice.service";
import { PagosService } from "src/app/services/pagos.service";
import { ActivatedRoute, Router } from "@angular/router";
import { AngularFirestore } from "@angular/fire/firestore";
import { Areasusuarios } from "src/app/interfaces/areasusuarios";
import { AuthserviceService } from "src/app/services/authservice.service";
import { Cursos } from "src/app/interfaces/cursos";
import { CoursesserviceService } from "src/app/services/coursesservice.service";
import { Cursousuario } from "src/app/interfaces/cursousuario";

@Component({
  selector: "app-register",
  templateUrl: "./register.component.html",
  styleUrls: ["./register.component.scss"],
})
export class RegisterComponent implements OnInit {
  public showError = {
    identity: true,
    firstName: true,
    lastName: true,
  };
  public inLoad: boolean = false;
  public thePassword: string = "";
  public registerData: Usuarios = {
    id: "",
    email: "",
    firstName: "",
    lastName: "",
    identity: "",
    phone: "",
    identityType: "dni",
    active: true,
    type: 2,
    cursos: [],
    date_create: undefined,
    date_edit: undefined,
    name_user_create: "",
    name_user_edit: "",
  };

  public registerPay: Pago = {
    id_user: "",
    ticket: "",
    comment: "",
  };
  public userData: Usuarios;
  public areasData: Areas[];
  public coursesData: Cursos[];
  public listPays: Pago[]; // pago registrados por cada usuario

  constructor(
    private areasservice: AreaserviceService,
    private coursesservice: CoursesserviceService,
    private usersservice: UsersService,
    public generalservice: GeneralserviceService,
    private pagosservice: PagosService,
    private route: ActivatedRoute,
    private afs: AngularFirestore,
    private authservice: AuthserviceService,
    private router: Router
  ) {}

  ngOnInit() {
    this.getAreas();
    this.getBooks();

    if (this.route.snapshot.paramMap.get("id") !== null) {
      this.inLoad = true;
      this.registerData.id = this.route.snapshot.paramMap.get("id");
      this.load();
    } else {
      this.inLoad = false;
      this.generateCode();
    }
    this.userData = this.authservice.getUserData();
  }

  generateCode() {
    let code = Math.floor(new Date().valueOf() * Math.random()).toString();
    this.registerData.code =
      code.substr(code.length - 6).substr(0, 3) +
      " " +
      code.substr(code.length - 6).substr(3, 3);
  }

  load() {
    this.usersservice.load(this.registerData.id).subscribe((data) => {
      this.registerData = data.data() as Usuarios;

      if (this.registerData.date_create !== undefined) {
        this.registerData.date_create = this.generalservice.convertDate(
          this.registerData.date_create
        );
      } else {
        this.registerData.date_create = null;
      }

      if (this.registerData.date_edit !== undefined) {
        this.registerData.date_edit = this.generalservice.convertDate(
          this.registerData.date_edit
        );
      } else {
        this.registerData.date_edit = null;
      }

      this.inLoad = false;
      this.registerPay.id_user = this.registerData.id;
      this.pagos();
      this.showError = {
        identity: this.generalservice.fieldError(
          "text",
          this.registerData.identity,
          8
        ),
        firstName: this.generalservice.fieldError(
          "text",
          this.registerData.firstName
        ),
        lastName: this.generalservice.fieldError(
          "text",
          this.registerData.lastName
        ),
        //phone: this.generalservice.fieldError("phone", this.registerData.phone),
        //email: this.generalservice.fieldError("email", this.registerData.email)
      };
    });
  }

  validStoreStep1() {
    this.inLoad = true;
    let pass = true;
    for (const prop in this.showError) {
      //console.log(prop)
      if (this.showError[prop]) {
        pass = false;
        this.inLoad = false;
        this.generalservice.eventShowAlert.emit({
          text: "Error en la validación de los campos",
          type: "error",
        });
      }
    }
    if (pass) this.checkIdentity();
  }

  checkIdentity() {
    this.usersservice
      .validIdentity(this.registerData.identityType, this.registerData.identity)
      .subscribe(
        (dataIdentity) => {
          //console.log(dataIdentity.docs)
          //console.log(dataIdentity.docs.length)
          if (dataIdentity.docs.length > 0) {
            let pass = true;
            if (this.registerData.id) {
              dataIdentity.docs.forEach((element) => {
                //console.log(element.data().id, this.registerData.id)
                if (element.data().id !== this.registerData.id) {
                  pass = false;
                }
              });
              //console.log(pass)
              if (!pass) {
                this.inLoad = false;
                this.generalservice.eventShowAlert.emit({
                  text:
                    "Error, el documento de indentidad ya se encuentra registrado",
                  type: "error",
                });
              } else {
                this.store();
                //this.checkEmailExist();
              }
            } else {
              this.inLoad = false;
              this.generalservice.eventShowAlert.emit({
                text:
                  "Error, el documento de indentidad ya se encuentra registrado",
                type: "error",
              });
            }
          } else {
            this.store();
            //this.checkEmailExist();
          }
        },
        (error) => {
          this.inLoad = false;
          this.generalservice.eventShowAlert.emit({
            text:
              "Error, en el proceso, por favor verifique su conexion a internet",
            type: "error",
          });
        }
      );
  }

  checkEmailExist() {
    this.usersservice.getBy("email", this.registerData.email).subscribe(
      (dataEmail) => {
        if (dataEmail.docs.length > 0) {
          let pass = true;
          if (this.registerData.id) {
            dataEmail.docs.forEach((element) => {
              if (element.data().email !== this.registerData.email) {
                pass = false;
              }
            });
            if (!pass) {
              this.inLoad = false;
              this.generalservice.eventShowAlert.emit({
                text: "Error, el correo electronico ya se encuentra registrado",
                type: "error",
              });
            } else {
              this.store();
            }
          } else {
            this.inLoad = false;
            this.generalservice.eventShowAlert.emit({
              text: "Error, el correo electronico ya se encuentra registrado",
              type: "error",
            });
          }
        } else {
          this.store();
        }
      },
      (error) => {
        this.inLoad = false;
        this.generalservice.eventShowAlert.emit({
          text:
            "Error, en el proceso, por favor verifique su conexion a internet",
          type: "error",
        });
      }
    );
  }

  store() {
    if (this.registerData.id) {
      this.registerData.id_user_edit = this.userData.id;
      this.registerData.name_user_edit =
        this.userData.firstName + " " + this.userData.lastName;
      this.registerData.date_edit = new Date();

      this.usersservice.update(this.registerData).then(
        () => {
          this.inLoad = false;
          this.generalservice.eventShowAlert.emit({
            text: "Se actualizaron los datos correctamente",
            type: "success",
          });
        },
        (error) => {
          this.inLoad = false;
          this.generalservice.eventShowAlert.emit({
            text: "Error en actualización de usuario",
            type: "danger",
          });
        }
      );
    } else {
      //console.log(this.registerData.type)
      const id = this.afs.createId();

      this.registerData.id_user_create = this.userData.id;
      this.registerData.name_user_create =
        this.userData.firstName + " " + this.userData.lastName;
      this.registerData.date_create = new Date();

      this.registerData.id_user_edit = null;
      this.registerData.name_user_edit = null;
      this.registerData.date_edit = null;

      let usernew = this.registerData;
      usernew.id = id;

      this.usersservice.store(usernew).then(
        () => {
          this.generalservice.eventShowAlert.emit({
            text: "Registro exitoso",
            type: "success",
          });
          this.registerData.id = usernew.id;
          this.registerPay.id_user = usernew.id;
          this.inLoad = false;
          //this.router.navigate(['admin','usuarios'])
        },
        (error) => {
          this.inLoad = false;
          //console.log(error)
          this.generalservice.eventShowAlert.emit({
            text: "Error en creación de usuario",
            type: "danger",
          });
        }
      );
    }
  }

  getAreas() {
    this.areasservice.list().subscribe((data) => {
      this.areasData = data as Areas[];
    });
  }

  getBooks() {
    this.coursesservice.list().subscribe((data) => {
      this.coursesData = data as Cursos[];
    });
  }

  storeArea(area: Areas) {
    const exists = this.registerData.areas.some((b) => b.id_area == area.id);

    if (!exists) {
      this.registerData.areas.push({ id_area: area.id, name_area: area.name });
    } else {
      this.generalservice.eventShowAlert.emit({
        text: "Esta area ya esta asignada al usuario",
        type: "info",
      });
    }
  }

  deleteArea(area: Areasusuarios) {
    let index = this.registerData.areas.indexOf(area);
    this.registerData.areas.splice(index, 1);
  }

  storeCurso(curso: Cursos) {
    let exists = false;
    if (this.registerData.cursos) {
      exists = this.registerData.cursos.some((b) => b.id_curso == curso.id);
    } else {
      this.registerData.cursos = [];
    }

    if (!exists) {
      this.registerData.cursos.push({
        id_curso: curso.id,
        name_curso: curso.name,
      });
    } else {
      this.generalservice.eventShowAlert.emit({
        text: "Este libro ya esta asignado al usuario",
        type: "info",
      });
    }
  }

  deleteCurso(curso: Cursousuario) {
    let index = this.registerData.cursos.indexOf(curso);
    this.registerData.cursos.splice(index, 1);
  }

  // PAGOS //
  pagos() {
    this.pagosservice.listPagos(this.registerData.id).subscribe((data) => {
      this.listPays = data as Pago[];
    });
  }

  checkPayExist() {
    this.inLoad = true;
    this.pagosservice.getBy("ticket", this.registerPay.ticket).subscribe(
      (dataPay) => {
        if (dataPay.docs.length > 0) {
          this.inLoad = false;
          this.generalservice.eventShowAlert.emit({
            text: "Error, el nro de boleta ya se encuentra registrado",
            type: "error",
          });
        } else {
          this.addPay();
        }
      },
      (error) => {
        this.inLoad = false;
        this.generalservice.eventShowAlert.emit({
          text:
            "Error, en el proceso, por favor verifique su conexion a internet",
          type: "error",
        });
      }
    );
  }

  addPay() {
    this.pagosservice.store(this.registerPay).then(
      (data) => {
        this.inLoad = false;
        this.generalservice.eventShowAlert.emit({
          text: "Pago registrado",
          type: "success",
        });
      },
      (error) => {
        this.inLoad = false;
        this.generalservice.eventShowAlert.emit({
          text: "Error en registro de pago",
          type: "danger",
        });
      }
    );
  }

  deletePay(pago) {
    this.pagosservice.delete(pago).then(
      () => {
        this.generalservice.eventShowAlert.emit({
          text: "Pago eliminado",
          type: "success",
        });
      },
      (error) => {
        this.generalservice.eventShowAlert.emit({
          text: "Error en eliminacion de pago",
          type: "danger",
        });
      }
    );
  }
}
