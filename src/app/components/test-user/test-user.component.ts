import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { PruebasService } from "src/app/services/pruebas.service";
import { Testusuarios } from "src/app/interfaces/testusuarios";
import * as ClassicEditor from "@ckeditor/ckeditor5-build-classic";
import { GeneralserviceService } from "src/app/services/generalservice.service";
import { Subscription } from "rxjs";
import { AreaserviceService } from "src/app/services/areaservice.service";
import { Areas } from "src/app/interfaces/areas";
import { CoursesserviceService } from "src/app/services/coursesservice.service";
import { Cursos } from "src/app/interfaces/cursos";
import { Cursousuario } from "src/app/interfaces/cursousuario";
import { Cursosactivos } from "src/app/interfaces/cursosactivos";

@Component({
  selector: "app-test-user",
  templateUrl: "./test-user.component.html",
  styleUrls: ["./test-user.component.scss"],
})
export class TestUserComponent implements OnInit {
  public Editor = ClassicEditor;
  public configEditor = { language: "es", isReadOnly: false };
  public inLoad: boolean = true;
  public resultSaveTest = {
    msg: "",
    saved: false,
    inSave: false,
  };

  public cursosData: Cursosactivos[] = [];
  public modalConfirmSaveTest: boolean = false;
  public modalConfirmPauseTest: boolean = false;
  public pruebaData: Testusuarios = {
    name: "Prueba",
    id_test: "",
    id_area: "",
    id_user: "",
    questions: [],
    score: 0,
    comments: "",
    status: 0,
    dateInitialize: null,
    dateRealize: null,
    dateEvaluate: null,
  };

  constructor(
    private route: ActivatedRoute,
    private pruebasservice: PruebasService,
    private generalservice: GeneralserviceService,
    private coursesservice: CoursesserviceService,
    private router: Router
  ) {}

  ngOnInit() {
    if (this.route.snapshot.paramMap.get("id") !== null) {
      this.pruebaData.id = this.route.snapshot.paramMap.get("id");
      this.loadData();
    }
  }

  loadData() {
    this.inLoad = true;
    this.pruebasservice.get(this.pruebaData.id).subscribe((data) => {
      let dataTemp = data.data() as Testusuarios;
      dataTemp.dateInitialize = this.generalservice.convertDate(
        dataTemp.dateInitialize
      );
      dataTemp.dateEvaluate = this.generalservice.convertDate(
        dataTemp.dateEvaluate
      );
      dataTemp.dateRealize = this.generalservice.convertDate(
        dataTemp.dateRealize
      );

      this.pruebaData = dataTemp;
      if (this.pruebaData.status === 1 || this.pruebaData.status === 2) {
        this.configEditor.isReadOnly = true;
        this.getcursosData();
      }
      this.inLoad = false;
    });
  }

  getcursosData() {
    let subscripcion: Subscription;
    subscripcion = this.coursesservice.listActives().subscribe(
      (data) => {
        this.cursosData = data as Cursosactivos[];
        this.cursosData.forEach((area) => {
          if (this.pruebaData.id_curso == area.id) {
            this.pruebaData.name_area = area.name;
          }
        });
        this.unsubscribeMethod(subscripcion);
      },
      (error) => {
        this.generalservice.eventShowAlert.emit({
          text:
            "Ha ocurrido un error cargando las areas, intente nuevamente o verifique su conexion a internet.",
          type: "error",
        });
      }
    );
  }

  modalConfirmPause() {
    this.modalConfirmPauseTest = true;
    this.resultSaveTest.msg = "";
    this.resultSaveTest.inSave = false;
    this.resultSaveTest.saved = false;
  }

  modalConfirmTest() {
    this.modalConfirmSaveTest = true;
    this.resultSaveTest.msg = "";
    this.resultSaveTest.inSave = false;
    this.resultSaveTest.saved = false;
  }

  saveTest() {
    this.resultSaveTest.inSave = true;
    this.pruebaData.status = 1;
    this.pruebaData.dateRealize = new Date();
    this.pruebasservice.store(this.pruebaData).then(
      () => {
        this.resultSaveTest.inSave = false;
        this.resultSaveTest.saved = true;
        this.generalservice.eventShowAlert.emit({
          text: "Prueba guardada exitosamente",
          type: "success",
        });
        this.router.navigate(["test", "usuario", "historial"]);
      },
      (error) => {
        this.generalservice.eventShowAlert.emit({
          text: "Ha ocurrido un error guardando la prueba, intente nuevamente",
          type: "error",
        });
      }
    );
  }

  pauseTest() {
    this.resultSaveTest.inSave = true;
    this.pruebaData.status = 0;
    this.pruebaData.dateRealize = new Date();
    this.pruebasservice.store(this.pruebaData).then(
      () => {
        this.resultSaveTest.inSave = false;
        this.resultSaveTest.saved = true;
        this.generalservice.eventShowAlert.emit({
          text: "Prueba guardada exitosamente",
          type: "success",
        });
        this.router.navigate(["test", "usuario"]);
      },
      (error) => {
        this.generalservice.eventShowAlert.emit({
          text: "Ha ocurrido un error guardando la prueba, intente nuevamente",
          type: "error",
        });
      }
    );
  }

  unsubscribeMethod(suscripcion: Subscription) {
    suscripcion.unsubscribe();
  }
}
