import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'typeUser'
})
export class TypeUserPipe implements PipeTransform {

  transform(value: any, ...args: any[]): any {
    let type: string
    switch (value) {
      case 1: type = 'Administrador / Profesor'; break
      case 2: type = 'Estandar'; break
      case 3: type = 'Profesor'; break
      default: break;
    }

    return type
  }

}
