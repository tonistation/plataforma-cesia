import { Pipe, PipeTransform } from "@angular/core";

@Pipe({
  name: "statusCoursesActive",
})
export class StatusCoursesActivePipe implements PipeTransform {
  transform(value: any, ...args: any[]): any {
    value = parseInt(value);
    switch (value) {
      case 1:
        return "Activo";
        break;
      case 2:
        return "Inactivo";
        break;
      default:
        return value;
    }
  }
}
