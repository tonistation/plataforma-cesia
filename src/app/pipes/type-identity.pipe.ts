import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'typeIdentity'
})
export class TypeIdentityPipe implements PipeTransform {

  transform(value: any, ...args: any[]): any {
    
    let type: string
    switch(value){
      case 'passport': type = 'Pasaporte'; break
      case 'dni': type = 'DNI'; break
      case 'ce': type = 'Carnet de Extranjeria'; break
      default: break;
    }

    return type;
  }

}
