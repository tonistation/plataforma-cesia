import { Pipe, PipeTransform } from "@angular/core";

@Pipe({
  name: "filterall",
})
export class FilterallPipe implements PipeTransform {
  transform(items: any[], field: string, value: string): any[] {
    let fieldA = field.split(",");

    if (!items) return [];
    if (!value || value.length == 0) return items;

    if (fieldA.length == 1) {
      return items.filter(
        (it) => it[fieldA[0]].toLowerCase().indexOf(value.toLowerCase()) != -1
      );
    } else if (fieldA.length == 2) {
      return items.filter(
        (it) =>
          it[fieldA[0]].toLowerCase().indexOf(value.toLowerCase()) != -1 ||
          it[fieldA[1]].toLowerCase().indexOf(value.toLowerCase()) != -1
      );
    } else {
      return items.filter(
        (it) =>
          it[fieldA[0]].toLowerCase().indexOf(value.toLowerCase()) != -1 ||
          it[fieldA[1]].toLowerCase().indexOf(value.toLowerCase()) != -1 ||
          it[fieldA[2]].toLowerCase().indexOf(value.toLowerCase()) != -1
      );
    }
  }
}
