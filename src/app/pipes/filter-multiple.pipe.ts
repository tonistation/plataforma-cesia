import { Pipe, PipeTransform } from "@angular/core";

@Pipe({
  name: "filterMultiple",
})
export class FilterMultiplePipe implements PipeTransform {
  transform(items: Array<any>, filter: {}): Array<any> {
    Object.keys(filter).forEach((key) => {
      if (filter[key] !== undefined && filter[key].id !== undefined)
        filter[key] = filter[key].id;

      return filter[key] === "" ||
        filter[key] === undefined ||
        filter[key] === null
        ? delete filter[key]
        : key;
    });

    //console.log(filter);

    /*return items.filter(item => {
      let notMatchingField = Object.keys(filter)
        .find(key => item[key] !== filter[key]);

      return !notMatchingField; // true if matches all fields
    });*/
    const filterKeys = Object.keys(filter);
    return items.filter((eachObj) => {
      return filterKeys.every((eachKey) => {
        if (!filter[eachKey].length) {
          return true; // passing an empty filter means that filter is ignored.
        }

        return eachObj[eachKey]
          .toLowerCase()
          .startsWith(filter[eachKey].toLowerCase());
      });
    });
  }
}
