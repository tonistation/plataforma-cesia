import { Pipe, PipeTransform } from "@angular/core";

@Pipe({
  name: "typematerials",
})
export class TypematerialsPipe implements PipeTransform {
  transform(value: any, ...args: any[]): any {
    switch (value) {
      case "ppt":
        return "Presentacion Power Point";
        break;
      case "pdf":
        return "Documento PDF";
        break;
      case "word":
        return "Documento Word";
        break;
      case "link":
        return "Vinculo";
        break;
      case "image":
        return "Imagen";
        break;
      case "other":
        return "Archivo";
        break;
      default:
        return value;
    }
  }
}
