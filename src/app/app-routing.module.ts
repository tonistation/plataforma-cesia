import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { DashboardComponent } from "./components/dashboard/dashboard.component";
import { LoginComponent } from "./components/shared/login/login.component";
import { DashboardAdminComponent } from "./components/admin/dashboard-admin/dashboard-admin.component";
import { SecureInnerPagesGuard } from "./shared/guard/secure-inner-pages.guard.ts.guard";
import { AuthGuard } from "./shared/guard/auth.guard";
import { RegisterComponent } from "./components/user/register/register.component";
import { CompleteregisterComponent } from "./components/user/completeregister/completeregister.component";
import { ListAreasComponent } from "./components/areas/list-areas/list-areas.component";
import { FormAreasComponent } from "./components/areas/form-areas/form-areas.component";
import { ListUserComponent } from "./components/user/list-user/list-user.component";
import { TestUserComponent } from "./components/test-user/test-user.component";
import { TestListComponent } from "./components/test-list/test-list.component";
import { TestListTeacherComponent } from "./components/test-list-teacher/test-list-teacher.component";
import { TestEvaluateTeacherComponent } from "./components/test-evaluate-teacher/test-evaluate-teacher.component";
import { UserProfileComponent } from "./components/shared/user-profile/user-profile.component";
import { ContactCompanyComponent } from "./components/shared/contact-company/contact-company.component";
import { BookComponent } from "./components/book/book.component";
import { BookReadComponent } from "./components/book-read/book-read.component";
import { BookuserComponent } from "./components/libros/bookuser/bookuser.component";
import { LoginAdminComponent } from "./components/shared/loginadmin/loginadmin.component";
import { ListbooksComponent } from "./components/listbooks/listbooks.component";
import { CoursesUserComponent } from "./components/courses-user/courses-user.component";
import { CoursesviewComponent } from "./components/coursesview/coursesview.component";
import { CourseComponent } from "./components/course/course.component";
import { CourselistComponent } from "./components/courselist/courselist.component";
import { CoursesactivelistComponent } from "./components/coursesactivelist/coursesactivelist.component";

const routes: Routes = [
  {
    path: "",
    component: DashboardComponent,
    canActivate: [AuthGuard],
    data: { rol: "all" },
  },
  {
    path: "login",
    component: LoginComponent,
    canActivate: [SecureInnerPagesGuard],
  },
  {
    path: "loginadmin",
    component: LoginAdminComponent,
    canActivate: [SecureInnerPagesGuard],
  },
  {
    path: "registro",
    component: CompleteregisterComponent,
    canActivate: [SecureInnerPagesGuard],
  },
  {
    path: "usuario",
    children: [
      { path: "", component: UserProfileComponent },
      { path: "perfil", component: UserProfileComponent },
    ],
    canActivate: [AuthGuard],
    data: { rol: "all" },
  },
  {
    path: "cursos",
    children: [
      { path: "", component: CoursesUserComponent },
      { path: ":code", component: CoursesviewComponent },
    ],
    canActivate: [AuthGuard],
    data: { rol: "all" },
  },
  {
    path: "contacto",
    children: [{ path: "", component: ContactCompanyComponent }],
    canActivate: [AuthGuard],
    data: { rol: "all" },
  },
  {
    path: "test",
    children: [
      { path: "", component: DashboardComponent },
      { path: "realizar/:id", component: TestUserComponent },
      { path: "detalle/:id", component: TestUserComponent },
      { path: "usuario", component: TestListComponent },
      { path: "usuario/:tab", component: TestListComponent },
    ],
    canActivate: [AuthGuard],
    data: { rol: "estandar" },
  },
  {
    path: "profesor",
    children: [
      { path: "", component: DashboardComponent },
      { path: "test/lista", component: TestListTeacherComponent },
      { path: "test/lista/:tab", component: TestListTeacherComponent },
      { path: "test/detalle/:id", component: TestEvaluateTeacherComponent },
      { path: "test/evaluar/:id", component: TestEvaluateTeacherComponent },
    ],
    canActivate: [AuthGuard],
    data: { rol: "teacher" },
  },
  {
    path: "admin",
    children: [
      { path: "", component: DashboardAdminComponent },
      { path: "usuarios", component: ListUserComponent },
      { path: "usuarios/nuevo", component: RegisterComponent },
      { path: "usuarios/editar/:id", component: RegisterComponent },
      { path: "areas", component: ListAreasComponent },
      { path: "areas/nuevo", component: FormAreasComponent },
      { path: "areas/editar/:id", component: FormAreasComponent },
      { path: "especialidades", component: CourselistComponent },
      { path: "especialidades/nuevo", component: CourseComponent },
      { path: "especialidades/editar/:id", component: CourseComponent },
    ],
    canActivate: [AuthGuard],
    data: { rol: "admin" },
  },
  {
    path: "admin/cursos",
    component: CoursesactivelistComponent,
    canActivate: [AuthGuard],
    data: { rol: "coord" },
  },
  {
    path: "**",
    redirectTo: "/",
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}

/*{
  path: 'prueba/:idprueba',
  component: PruebaComponent,
  canActivate: [SecureInnerPagesGuard]
}*/
