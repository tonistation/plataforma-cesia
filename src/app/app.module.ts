import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { ClarityModule } from "@clr/angular";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { AngularFireModule } from "@angular/fire";
import { AngularFirestoreModule } from "@angular/fire/firestore";
import { AngularFireStorageModule } from "@angular/fire/storage";
import { AngularFireAuthModule } from "@angular/fire/auth";
import { environment } from "src/environments/environment.prod";
import { HttpClientModule } from "@angular/common/http";
import { HeaderComponent } from "./components/shared/header/header.component";
import { DashboardComponent } from "./components/dashboard/dashboard.component";
import { LoginComponent } from "./components/shared/login/login.component";
import { LoginAdminComponent } from "./components/shared/loginadmin/loginadmin.component";
import { DashboardAdminComponent } from "./components/admin/dashboard-admin/dashboard-admin.component";
import { RegisterComponent } from "./components/user/register/register.component";
import { CompleteregisterComponent } from "./components/user/completeregister/completeregister.component";
import { AlertComponent } from "./components/shared/alert/alert.component";
import { ListAreasComponent } from "./components/areas/list-areas/list-areas.component";
import { FormAreasComponent } from "./components/areas/form-areas/form-areas.component";
import { ListUserComponent } from "./components/user/list-user/list-user.component";
import { TypeIdentityPipe } from "./pipes/type-identity.pipe";
import { CKEditorModule } from "@ckeditor/ckeditor5-angular";
import { TestUserComponent } from "./components/test-user/test-user.component";
import { TestListComponent } from "./components/test-list/test-list.component";
import { TestListTeacherComponent } from "./components/test-list-teacher/test-list-teacher.component";
import { TestEvaluateTeacherComponent } from "./components/test-evaluate-teacher/test-evaluate-teacher.component";
import { TypeUserPipe } from "./pipes/type-user.pipe";
import { MenuMobileComponent } from "./components/menu-mobile/menu-mobile.component";
import { NgxMaskModule, IConfig } from "ngx-mask";
import { FilterStatusTestUserPipe } from "./pipes/filter-status-test-user.pipe";
import { NgxPaginationModule } from "ngx-pagination";
import { FilterMultiplePipe } from "./pipes/filter-multiple.pipe";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { UserProfileComponent } from "./components/shared/user-profile/user-profile.component";
import { ContactCompanyComponent } from "./components/shared/contact-company/contact-company.component";
import { TimeagoPipe } from "./pipes/timeago.pipe";
import { HistoryTestComponent } from "./components/history-test/history-test.component";
import { BookComponent } from "./components/book/book.component";
import { BookReadComponent } from "./components/book-read/book-read.component";
import { BookuserComponent } from "./components/libros/bookuser/bookuser.component";
import { ListbooksComponent } from "./components/listbooks/listbooks.component";
import { CoursesUserComponent } from "./components/courses-user/courses-user.component";
import { CoursesviewComponent } from "./components/coursesview/coursesview.component";
import { CourselistComponent } from "./components/courselist/courselist.component";
import { CourseComponent } from "./components/course/course.component";
import { TypematerialsPipe } from "./pipes/typematerials.pipe";
import { CoursesactivelistComponent } from "./components/coursesactivelist/coursesactivelist.component";
import { SwiperModule } from "ngx-swiper-wrapper";
import { StatusCoursesActivePipe } from "./pipes/status-courses-active.pipe";
//import { AutocompleteLibModule } from "angular-ng-autocomplete";
import { UiSwitchModule } from "ngx-ui-switch";
import { FilterallPipe } from "./pipes/filterall.pipe";
import { SelectDropDownModule } from "ngx-select-dropdown";

export const options: Partial<IConfig> | (() => Partial<IConfig>) = {};

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    DashboardComponent,
    LoginComponent,
    LoginAdminComponent,
    DashboardAdminComponent,
    RegisterComponent,
    CompleteregisterComponent,
    AlertComponent,
    ListAreasComponent,
    FormAreasComponent,
    ListUserComponent,
    TypeIdentityPipe,
    TestUserComponent,
    TestListComponent,
    TestListTeacherComponent,
    TestEvaluateTeacherComponent,
    TypeUserPipe,
    MenuMobileComponent,
    FilterStatusTestUserPipe,
    FilterMultiplePipe,
    UserProfileComponent,
    ContactCompanyComponent,
    TimeagoPipe,
    HistoryTestComponent,
    BookComponent,
    BookReadComponent,
    BookuserComponent,
    ListbooksComponent,
    CoursesUserComponent,
    CoursesviewComponent,
    CourselistComponent,
    CourseComponent,
    TypematerialsPipe,
    CoursesactivelistComponent,
    StatusCoursesActivePipe,
    FilterallPipe,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ClarityModule,
    BrowserAnimationsModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFirestoreModule, // imports firebase/firestore, only needed for database features
    AngularFireAuthModule, // imports firebase/auth, only needed for auth features,
    AngularFireStorageModule, // imports firebase/storage only needed for storage features
    HttpClientModule,
    FormsModule,
    CKEditorModule,
    NgxMaskModule.forRoot(options),
    NgxPaginationModule,
    FormsModule,
    ReactiveFormsModule,
    SwiperModule,
    //AutocompleteLibModule,
    UiSwitchModule,
    SelectDropDownModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
