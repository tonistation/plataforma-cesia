import { Injectable } from "@angular/core";
import {
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  UrlTree,
  Router,
} from "@angular/router";
import { Observable } from "rxjs";
import { AuthserviceService } from "src/app/services/authservice.service";

@Injectable({
  providedIn: "root",
})
export class AuthGuard implements CanActivate {
  constructor(public authService: AuthserviceService, public router: Router) {}

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<boolean> | Promise<boolean> | boolean {
    if (this.authService.isLoggedIn) {
      let rol = next.data.rol as string;
      let userData = this.authService.getUserData();

      console.log(rol, userData.type);

      switch (rol) {
        case "admin":
          return userData.type === 1;
          break;
        case "coord":
          return userData.type === 1 || userData.type === 4;
          break;
        case "estandar":
          return userData.type === 2;
          break;
        case "teacher":
          return (
            userData.type === 1 || userData.type === 3 || userData.type === 4
          );
          break;
        case "all":
          return true;
          break;
        default:
          return false;
          break;
      }
      return false;
    } else {
      this.router.navigate(["/login"]);
      return false;
    }
  }
}
