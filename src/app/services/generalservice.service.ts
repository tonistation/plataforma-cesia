import { Injectable, EventEmitter } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class GeneralserviceService {

  eventShowAlert = new EventEmitter()

  constructor(
    private http: HttpClient
  ) { }

  getPicOfDay() {

    return this.http.get("https://cors-anywhere.herokuapp.com/https://www.bing.com/HPImageArchive.aspx?format=js&idx=0&n=1&mkt=es-MX")
  }

  fieldError(type: string, value: string, min: number = 0) {
    let error = true
    switch (type) {
      case 'text':
        if (value.trim() !== '') {
          error = false
        }
        if (min > 0 && value.length >= min) {
          error = false
        }
        break;
      case 'phone':
        let phonevalid = /^\(?([0-9]{1,3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{3})$/;
        if (value !== undefined) {
          if (value.match(phonevalid)) {
            error = false
          }
        }
        break;
      case 'email':
        let emailvalid = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
        if (value !== undefined) {
          if (value.match(emailvalid)) {
            error = false
          }
        }
        break;
      default:
        break;
    }
    return error
  }

  convertDate(dateTS: any) {
    if (dateTS !== null && dateTS !== undefined) { 
      if(dateTS.seconds !== null && dateTS.seconds !== undefined){
        let unixtimestamp = dateTS.seconds;
        let date = new Date(unixtimestamp * 1000) 
        return date
      }else{
        return dateTS
      } 
      
    } else {
      return dateTS
    }
  }
}
