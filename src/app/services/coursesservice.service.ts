import { Injectable } from "@angular/core";
import {
  AngularFirestore,
  AngularFirestoreCollection,
} from "@angular/fire/firestore";
import { Cursos } from "../interfaces/cursos";
import { BanksService } from "./banks.service";
import { Videos } from "../interfaces/videos";
import { Cursosactivos } from "../interfaces/cursosactivos";

@Injectable({
  providedIn: "root",
})
export class CoursesserviceService {
  constructor(
    private afs: AngularFirestore,
    private bankservice: BanksService
  ) {}

  list() {
    let areasRef: AngularFirestoreCollection = this.afs.collection("cursos");
    return areasRef.valueChanges();
  }

  listActives() {
    let areasRef: AngularFirestoreCollection = this.afs.collection(
      "cursos_activos"
    );
    return areasRef.valueChanges();
  }

  listActivesByTypeUser(idUser: string, type: number) {
    if (type == 1) {
      let cursoRef: AngularFirestoreCollection = this.afs.collection(
        "cursos_activos"
      );
      return cursoRef.valueChanges();
    } else if (type == 4) {
      const cursoRef = this.afs.collection("cursos_activos", (ref) =>
        ref.where("id_coordinador", "==", idUser)
      );
      return cursoRef.valueChanges();
    }
  }

  getByValues(field: string, value: string | number) {
    const userRef = this.afs.collection("cursos_activos", (ref) =>
      ref.where(field, "==", value)
    );
    return userRef.valueChanges();
  }

  editNameActive(data: Cursosactivos) {
    const collection = this.afs.collection("cursos_activos").doc(data.id);
    return collection.set({ name: data.name }, { merge: true });
  }

  editCoordActive(data: Cursosactivos) {
    const collection = this.afs.collection("cursos_activos").doc(data.id);
    return collection.set(
      {
        id_coordinador: data.id_coordinador,
        name_coordinador: data.name_coordinador,
      },
      { merge: true }
    );
  }

  editActive(id: string, data: Partial<Cursosactivos>) {
    const collection = this.afs.collection("cursos_activos").doc(id);
    return collection.set(data, { merge: true });
  }

  get(cursoId: string) {
    let areasRef = this.afs.collection("cursos").doc(cursoId);
    return areasRef.get();
  }

  getActive(cursoId: string) {
    let areasRef = this.afs.collection("cursos_activos").doc(cursoId);
    return areasRef.get();
  }

  store(data: Cursos) {
    const cursosCollection = this.afs.collection("cursos");
    //const id = this.afs.createId();
    //data.id = id;

    data.pruebas.forEach((test) => {
      if (test.id_bank === undefined || test.id_bank === "") {
        const id_bank = this.afs.createId();
        test.id_bank = id_bank;
        this.bankservice.store({ id: id_bank, questions: [] });
      }

      if (test.id === undefined || test.id === "") {
        test.id = this.afs.createId();
      }
    });

    return cursosCollection.doc(data.id).set(data);
  }

  storeCursoActivo(data) {
    console.log(data);

    const cursosCollection = this.afs.collection("cursos_activos");
    const id = this.afs.createId();
    data.id = id;
    return cursosCollection.doc(id).set(data);
  }

  storeInCursoActivo(id: string, data: Partial<Cursosactivos>) {
    console.log(id, data);

    const collection = this.afs.collection("cursos_activos").doc(id);
    return collection.set(data, { merge: true });
  }

  storeInCurso(id: string, data: Partial<Cursos>) {
    console.log(data);

    const collection = this.afs.collection("cursos").doc(id);
    return collection.set(data, { merge: true });
  }

  update(data: Cursos) {
    data.pruebas.forEach((test) => {
      if (test.id_bank === undefined || test.id_bank === "") {
        const id_bank = this.afs.createId();
        test.id_bank = id_bank;
        this.bankservice.store({ id: id_bank, questions: [] });
      }

      if (test.id === undefined || test.id === "") {
        test.id = this.afs.createId();
      }
    });

    return this.afs.collection("cursos").doc(data.id).set(data);
  }

  delete(areaId: string) {
    let areasRef = this.afs.collection("cursos").doc(areaId);
    return areasRef.delete();
  }

  deleteActive(cursoId: string) {
    let areasRef = this.afs.collection("cursos_activos").doc(cursoId);
    return areasRef.delete();
  }
}
