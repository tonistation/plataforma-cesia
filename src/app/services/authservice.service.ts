import { Injectable, NgZone } from "@angular/core";
import { AngularFireAuth } from "@angular/fire/auth";
import {
  AngularFirestore,
  AngularFirestoreDocument,
  AngularFirestoreCollection,
} from "@angular/fire/firestore";
import { Router } from "@angular/router";
import { Usuarios } from "../interfaces/usuarios";
import { GeneralserviceService } from "./generalservice.service";
import { Subscription } from "rxjs";

@Injectable({
  providedIn: "root",
})
export class AuthserviceService {
  userData: any; // Save logged in user data
  private isRegister: boolean = false;

  constructor(
    public afs: AngularFirestore, // Inject Firestore service
    public afAuth: AngularFireAuth, // Inject Firebase auth service
    public router: Router,
    public ngZone: NgZone,
    public generalservice: GeneralserviceService
  ) {
    this.afAuth.authState.subscribe((user) => {
      if (user) {
        //console.log(user);
        //console.log(JSON.parse(localStorage.getItem('user')))
        /*console.log(
          localStorage.getItem("proccess") === null
            ? "login"
            : localStorage.getItem("proccess")
        );*/
        const userProccess =
          localStorage.getItem("proccess") === null
            ? "login"
            : localStorage.getItem("proccess");

        var documento = user.email.substr(0, user.email.indexOf("@"));
        //console.log(documento);

        const userRef = this.afs.collection("usuarios", (ref) =>
          ref.where("identity", "==", documento)
        );

        userRef.valueChanges().subscribe((dataExtra) => {
          this.userData = user;
          console.log(dataExtra);
          localStorage.setItem("user", JSON.stringify(this.userData));

          localStorage.setItem("userDataExtra", JSON.stringify(dataExtra));

          if (
            this.router.url === "/login" ||
            this.router.url === "/registro" ||
            this.router.url === "/loginadmin"
          )
            this.router.navigate([""]);
        });

        //JSON.parse(localStorage.getItem('user'));
      } else {
        localStorage.setItem("user", null);
        localStorage.setItem("userDataExtra", null);
        //JSON.parse(localStorage.getItem('user'));
      }
    });
  }

  unsubscribeMethod(suscripcion: Subscription) {
    suscripcion.unsubscribe();
  }

  // Sign in with email/password
  LogIn(email: string, password: string) {
    email = email + "@esconfa.com";
    this.isRegister = false;
    localStorage.setItem("proccess", "login");
    return this.afAuth.auth.signInWithEmailAndPassword(email, password);
  }

  // Sign up with email/password
  Register(email: string, password: string) {
    return this.afAuth.auth
      .createUserWithEmailAndPassword(email, password)
      .then((result) => {
        //this.SendVerificationMail();
      })
      .catch((error) => {
        console.log(error);

        this.generalservice.eventShowAlert.emit({
          text: "Error registrando usuario",
          type: "error",
        });
      });
  }

  // Send email verfificaiton when new user sign up
  SendVerificationMail() {
    return this.afAuth.auth.currentUser.sendEmailVerification().then(() => {
      this.router.navigate(["verify-email-address"]);
    });
  }

  // Reset Forggot password
  ForgotPassword(passwordResetEmail) {
    return this.afAuth.auth
      .sendPasswordResetEmail(passwordResetEmail)
      .then(() => {
        window.alert(
          "Se ha enviado a su correo electronico un link para cambiar su contraseña"
        );
      })
      .catch((error) => {
        window.alert(error);
      });
  }

  // Returns true when user is looged in and email is verified
  get isLoggedIn(): boolean {
    const user = JSON.parse(localStorage.getItem("user"));
    return user !== null ? true : false;
  }

  getUserData() {
    const user = JSON.parse(localStorage.getItem("userDataExtra"));
    console.log(user);
    //const userRef: AngularFirestoreCollection = this.afs.collection('usuarios', ref => ref.where('id', '==', user.uid) )
    return user[0];
  }

  /* Setting up user data when sign in with username/password, 
  sign up with username/password and sign in with social auth  
  provider in Firestore database using AngularFirestore + AngularFirestoreDocument service */

  SetUserData(user) {
    const userRef: AngularFirestoreDocument<any> = this.afs.doc(
      `usuarios/${user.uid}`
    );
    return userRef.set(user, {
      merge: true,
    });
  }

  // Sign out
  LogOut() {
    return this.afAuth.auth.signOut().then(() => {
      localStorage.removeItem("user");
      this.router.navigate(["login"]);
    });
  }

  getUser(id_user) {
    const userRef = this.afs.collection("usuarios").doc(id_user);
    return userRef.get();
  }

  checkDocumento(nroBoleta) {
    const searchBoleta = this.afs.collection("usuarios", (ref) =>
      ref.where("identity", "==", nroBoleta)
    );
    return searchBoleta.get();
  }

  //616 475

  checkRegisterCode(code: string) {
    const searchCode = this.afs.collection("usuarios", (ref) =>
      ref.where("code", "==", code)
    );
    return searchCode.get();
  }
}
