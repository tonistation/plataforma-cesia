import { Injectable } from "@angular/core";
import { AngularFirestore } from "@angular/fire/firestore";
import { Usuarios } from "../interfaces/usuarios";

@Injectable({
  providedIn: "root",
})
export class UsersService {
  constructor(private afs: AngularFirestore) {}

  list() {
    return this.afs.collection("usuarios").valueChanges();
  }

  list_get() {
    return this.afs.collection("usuarios").get();
  }

  listEstandar() {
    return this.afs
      .collection("usuarios", (usuario) => usuario.where("type", "==", 2))
      .valueChanges();
  }

  get(id: string) {
    let userRef = this.afs.collection("usuarios").doc(id);
    return userRef.get();
  }

  getBy(field: string, value: string | number) {
    const userRef = this.afs.collection("usuarios", (ref) =>
      ref.where(field, "==", value)
    );
    return userRef.get();
  }

  validIdentity(identityType: string, identity: string) {
    const userRef = this.afs.collection("usuarios", (ref) =>
      ref
        .where("identityType", "==", identityType)
        .where("identity", "==", identity)
    );
    return userRef.get();
  }

  // cargar un registro
  load(id: string) {
    return this.afs.collection("usuarios").doc(id).get();
  }

  store(data: Usuarios) {
    console.log(data);

    const usersCollection = this.afs.collection("usuarios");
    return usersCollection.doc(data.id).set(data);
  }

  update(data: Usuarios) {
    return this.afs.collection("usuarios").doc(data.id).set(data);
  }

  setactive(data: Usuarios) {
    return this.afs.collection("usuarios").doc(data.id).update({
      active: false,
    });
  }
}
