import { Injectable } from "@angular/core";
import {
  AngularFirestore,
  AngularFirestoreCollection
} from "@angular/fire/firestore";
import { Libros } from "../interfaces/libros";

@Injectable({
  providedIn: "root"
})
export class BooksService {
  constructor(private afs: AngularFirestore) {}

  list() {
    let areasRef: AngularFirestoreCollection = this.afs.collection("libros");
    return areasRef.valueChanges();
  }

  get(bookId: string) {
    let areasRef = this.afs.collection("libros").doc(bookId);
    return areasRef.get();
  }

  store(data: Libros) {
    const areasCollection = this.afs.collection("libros");
    const id = this.afs.createId();
    data.id = id;
    return areasCollection.doc(id).set(data);
  }

  update(data: Libros) {
    return this.afs
      .collection("libros")
      .doc(data.id)
      .set(data);
  }

  delete(areaId: string) {
    let areasRef = this.afs.collection("libros").doc(areaId);
    return areasRef.delete();
  }
}
