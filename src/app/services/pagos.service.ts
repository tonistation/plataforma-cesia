import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Pago } from '../interfaces/pago';

@Injectable({
  providedIn: 'root'
})
export class PagosService {

  constructor(
    private afs: AngularFirestore
  ) { }

  // cargar un pagos por cliente
  listPagos(id) {
    let pagos = this.afs.collection('pagos', ref => ref.where('id_user', '==', id));
    return pagos.valueChanges()
  }

  getBy(field: string, value: string) {
    const userRef = this.afs.collection('pagos', ref => ref.where(field, '==', value))
    return userRef.get();
  }

  store(data: Pago) {
    const usersCollection = this.afs.collection('pagos')
    const id = this.afs.createId()
    data.id = id
    return usersCollection.doc(id).set(data)
  }

  delete(data: Pago) {
    return this.afs.collection("pagos").doc(data.id).delete()
  }
}
