export const environment = {
  production: true,
  name_company: "ESCONFA INTERNACIONAL",
  firebase: {
    apiKey: "AIzaSyAGIfIQi2hVfj4UzivazDj94ZocK_CGMmM",
    authDomain: "plataforma-evaluacion.firebaseapp.com",
    databaseURL: "https://plataforma-evaluacion.firebaseio.com",
    projectId: "plataforma-evaluacion",
    storageBucket: "plataforma-evaluacion.appspot.com",
    messagingSenderId: "885046006968",
    appId: "1:885046006968:web:159780ee2f2f21b28e504d",
    measurementId: "G-83SHK0WFCN",
  },
};
