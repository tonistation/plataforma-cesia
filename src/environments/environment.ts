// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  name_company: "ESCONFA INTERNACIONAL",
  firebase: {
    apiKey: "AIzaSyAGIfIQi2hVfj4UzivazDj94ZocK_CGMmM",
    authDomain: "plataforma-evaluacion.firebaseapp.com",
    databaseURL: "https://plataforma-evaluacion.firebaseio.com",
    projectId: "plataforma-evaluacion",
    storageBucket: "plataforma-evaluacion.appspot.com",
    messagingSenderId: "885046006968",
    appId: "1:885046006968:web:159780ee2f2f21b28e504d",
    measurementId: "G-83SHK0WFCN",
  },
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
